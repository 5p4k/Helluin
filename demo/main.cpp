//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 06/08/16.
//

#include "idx.h"
#include "iterators.h"
#include <iostream>
#include <vector>
#include <graph_adj_list.h>
#include "proxies.h"

using namespace he;

using remap_idx_cit = map_item_iterator<const std::vector<int>, std::vector<std::size_t>::const_iterator>;
using remap_idx_it = map_item_iterator<std::vector<int>, std::vector<std::size_t>::const_iterator>;

struct my_it : remap_iterator_base<my_it, std::vector<int>::iterator> {
    using remap_iterator_base<my_it, std::vector<int>::iterator>::remap_iterator_base;
};

struct my_cit : remap_iterator_base<my_cit, std::vector<int>::const_iterator> {
    using remap_iterator_base<my_cit, std::vector<int>::const_iterator>::remap_iterator_base;
};

struct test_fwd_iterator {
    using value_type = void;
    using reference = void;
    using iterator_category = std::forward_iterator_tag;
    using difference_type  = std::ptrdiff_t;
    using pointer = void;

    inline test_fwd_iterator &operator++() {
        return *this;
    }

    inline test_fwd_iterator operator++(int) {
        return *this;
    }
};

struct my_fwd_it : remap_iterator_base<my_fwd_it, test_fwd_iterator> {
    using remap_iterator_base<my_fwd_it, test_fwd_iterator>::remap_iterator_base;
};

void foobar() {
    std::vector<int> values = {0, 2, 4, 6, 8, 10, 12, 14, 16};
    std::vector<std::size_t> indices = {0, 3, 4};
    for (remap_idx_it it(values, indices.begin()); it != indices.end(); ++it) {
        remap_idx_cit cit = it;
        *it += 1;
        std::cout << *cit << std::endl;
    }

}

int test_compile_remap_it() {

    std::vector<int> arr = {1, 2, 4};

    my_it i = my_it(arr.begin());
    my_it j = my_it(arr.end());
    my_cit k = my_cit(arr.end());

    std::cout << (i == j) << std::endl;
    std::cout << (i != j) << std::endl;

    std::cout << (i - j) << std::endl;
    std::cout << (i - arr.end()) << std::endl;
    std::cout << (i - k) << std::endl;
    std::cout << (k - arr.end()) << std::endl;
    std::cout << (k - j) << std::endl;

    ++i;
    j++;
    --i;
    j--;

    ++k;
    --k;
    k++;
    k--;

    i += 2;
    j -= 2;
    k += 2;
    k -= 2;

    test_fwd_iterator abc;
    my_fwd_it def(abc);

    ++def;

    return 4;
}

int test_idx_stuff() {
    idx<int> a;
    (void) a;

    idx<long> b = a;

//   idx<char *> c = b;

//   (void)c;

    a = invalid_idx;

    (void) b;

    std::cout << std::boolalpha << std::is_convertible<int, long>::value << std::endl;

    return 4;
}

void instantiate_stuff() {
    proxy::node_proxy<adj_list_graph<>> a;
    (void) a;
    proxy::node_const_proxy<adj_list_graph<>> b(a);
    (void) b;
    proxy::arc_proxy<adj_list_graph<>> c;
    (void) c;
    proxy::arc_const_proxy<adj_list_graph<>> d(c);
    (void) d;
}

int main() {
    return 0;
}
