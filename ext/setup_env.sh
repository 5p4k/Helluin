#!/usr/bin/env bash

if [[ -z $CI_BUILD_NAME ]] || [[ -z $CI_BUILD_REF ]] || [[ -z $CI_BUILD_STAGE ]]; then
    echo "CI_BUILD_NAME or CI_BUILD_REF undefined; are you in a CI build?"
    exit 1
fi

# Set default build dir
[[ -n $BUILD_DIR ]]   || BUILD_DIR="cmake-build-debug"
[[ -n $BUILD_FLAGS ]] || BUILD_FLAGS=""

# Give a name to the artifacts
BUILD_ID="build-${CI_BUILD_REF:0:7}"

# Check which software is missing
TO_INSTALL=()
PATCH_LIBCXX=0

if [[ $CI_BUILD_NAME == *"clang"* ]]; then
    TO_INSTALL+=(libc++1)
    if [[ $CI_BUILD_STAGE == "build" ]]; then
        TO_INSTALL+=(libc++-dev)
        PATCH_LIBCXX=1
        BUILD_FLAGS+="-DCMAKE_CXX_FLAGS=\"-stdlib=libc++\""
    fi
fi

if [[ $CI_BUILD_STAGE == "build" ]] || [[ $CI_BUILD_NAME == *"ctest"* ]]; then
    TO_INSTALL+=(valgrind)
fi

if [[ ${#TO_INSTALL[@]} -gt 0 ]]; then
    echo "Updating aptitude to install ${#TO_INSTALL[@]} elements..."
    apt-get update -qqy
    echo "Installing ${TO_INSTALL[@]}..."
    apt-get install -qqy ${TO_INSTALL[@]}
else
    echo "Nothing to install."
fi

# Check if this version of libc++ is broken
if [[ $PATCH_LIBCXX -ne 0 ]]; then
    if [[ $(sha256sum /usr/include/c++/v1/string | cut -d ' ' -f 1) == \
        "b3d90ad76ded9168bbd691b0e355a45c9947aabb8dc4991d18b2605f4b636b72" ]];
    then
        echo "Found broken implementation of string in libc++; patching..."
        curl https://gitlab.com/snippets/23614/raw | patch /usr/include/c++/v1/string
    fi
fi

export BUILD_DIR
export BUILD_ID
export BUILD_FLAGS

