Graph library for C++
==========================

[![build status](https://gitlab.com/5p4k/Helluin/badges/master/build.svg)](https://gitlab.com/5p4k/Helluin/commits/master)
[![coverage report](https://gitlab.com/5p4k/Helluin/badges/master/coverage.svg)](https://gitlab.com/5p4k/Helluin/commits/master)

[Doxygen documentation](https://5p4k.gitlab.io/Helluin/)

Tests coverage breakdown
-------------------------
Tested configurations:
 - `clang 3.8` + `libc++`
 - `gcc 5.2` (standard library)
 - `valgrind` for memory check.

| UT (clang) | UT (gcc) | All tests (clang) | All tests (gcc) |
|:----------:|:--------:|:-----------------:|:---------------:|
| [![coverage report, unit test with clang + libc++](https://gitlab.com/5p4k/Helluin/badges/master/coverage.svg?job=unit_test:clang)](https://gitlab.com/5p4k/Helluin/commits/master) | [![coverage report, unit test with gcc](https://gitlab.com/5p4k/Helluin/badges/master/coverage.svg?job=unit_test:gcc)](https://gitlab.com/5p4k/Helluin/commits/master) | [![coverage report, other tests with clang + libc++](https://gitlab.com/5p4k/Helluin/badges/master/coverage.svg?job=ctests:clang)](https://gitlab.com/5p4k/Helluin/commits/master) | [![coverage report, other tests with gcc](https://gitlab.com/5p4k/Helluin/badges/master/coverage.svg?job=ctests:gcc)](https://gitlab.com/5p4k/Helluin/commits/master) |

Project structure
-----------------
 - `./src`: source code of the main library
    - `./src/html`, `./src/latex` compiled doxygen documentation 
 - `./demo`: source code of the demo program
 - `./unit_test`: source code of the unit test program
 - `./ext`: extra files and external projects.
   - `./ext/catch`: dynamically fetched [Catch](https://github.com/philsquared/Catch) sources.
 - `./bin`: output folder for executable binaries.
 - `./lib`: output folder for libraries.

Wishlist
--------
 - Graph representation separated from graph logic.
 - Non-templated abstract graph class, to write algorithm that are agnosic w.r.t. graph representation and type.
 - Access to nodes and arcs happens via persistent, strongly typed indices.
 - Possibility of storing per-node/per-arc data by attaching storage units, which are dynamically kept in sync with the
   pools of arc and node indices.
 - Graph can exist without need of storing any specific data on edges or vertices, just referring them by index.
 - Offer range iterability and iterators wherever possible, for accessing neighbors and adjacent arcs.
 - Hide the graph base class behind lightweight proxies that offer OOP interaction with the graph objects.
 