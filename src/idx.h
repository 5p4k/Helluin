//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 06/08/16.
//

/** @file idx.h
 * @brief Strongly typed index.
 */

#ifndef HE_IDX_H
#define HE_IDX_H

#include <type_traits>
#include <ostream>
#include <limits>
#include <cstdint>

/** @namespace he */
namespace he {

    /** @brief The underlying chosen integer type for all arcs and nodes.
     *
     * The maximum value <code>std::numeric_limits<weak_idx_t>::max()</code> is used as a signaling value throughout the
     * whole library to denote an invalid index.
     *
     * @todo For now, this has been set to a fast 32bit integer type. Consider using `std::size_t`.
     */
    using weak_idx_t = std::uint_fast32_t;

    /** @brief Strongly typed generic index class.
     *
     * This class can *implicitly* convert *from*:
     *  - any other `idx<U>` type, such that `U` is convertible to `T`.
     *
     * It can *explicitly* convert *to*:
     *  - any other `idx<U>` type, such that `T` is a base class of `U` (a downcast).
     *  - a `bool`, which evaluates to `true` if and only if the represented type is less than the maximum value
     *    representable by @ref he::weak_idx_t.
     *
     * It can *implicitly* convert *to*:
     *  - a @ref he::weak_idx_t.
     *
     * Moreover, it can be assigned from any @ref idx<void>, which will set it to the maximum value representable by
     * a @ref he::weak_idx_t, which is considered an invalid index.
     *
     * Only @ref graph_base and subclasses are allowed to create `idx<T>` *from* @ref weak_idx_t.
     *
     * @note All `idx<T>` classes can be compared with `==` and `!=` to any numeric type or to another `idx<U>` class.
     * The `operator<<` is overloaded for `idx<T>`.
     *
     * @tparam T The type for which this class is strongly typed.
     */
    template<class T>
    struct idx {
        /** @brief Alias to the represented type `T`.
         */
        using value_type = T;

        /** @{ */

        /** @brief Default initializes the struct with the maximum value for a @ref he::weak_idx_t (an invalid index).
         */
        inline idx() noexcept;

        /** @brief Default initializes the struct with the maximum value for a @ref he::weak_idx_t (an invalid index).
         */
        inline idx(idx<void>) noexcept;

        /** @brief Sets the struct to the the maximum value for a @ref he::weak_idx_t (an invalid index).
         */
        inline idx &operator=(idx<void>);

        /** @} */

        /** @brief Converts the struct to a boolean.
         *
         * The index is considered `true` if and only if its @ref he::weak_idx_t representation is less than
         * <code>std::numeric_limits<weak_idx_t>::max()</code>.
         */
        inline explicit operator bool() const;

        /** @brief Converts the struct back to the non-strongly-typed integer representation.
         */
        inline operator weak_idx_t() const;

        /** @brief Allows conversion from a convertible type `U` to `idx<T>`.
         */
        template<class U, class = typename std::enable_if<
                std::is_convertible<U, T>::value or std::is_base_of<U, T>::value, U>::type>
        inline idx(idx<U> other) noexcept;

        /** @brief Allows explicit downcasting to `idx<U>`, if `U` is a subclass of `T`.
         */
        template<class U, class = typename std::enable_if<std::is_base_of<T, U>::value>::type>
        inline explicit operator idx<U>() const;

    protected:
        friend class graph_base;

        template<class U>
        friend
        struct idx;

        /** @brief Initializes a class with index value @p i.
         */
        inline idx(weak_idx_t i) noexcept : _i(i) {};

    private:
        weak_idx_t _i;
    };

    /** @brief Template specialization for `void`.
     *
     * A `idx<void>` class is intended to represent no index at all. This is just an empty class which just casts to the
     * maximum representable value for @ref he::weak_idx_t, which is an invalid index.
     *
     * You can create it, assign it to any other @ref idx type, with the result of setting it to an invalid index.
     * To this end, there is a static constant defined which shall serve as *the* invalid strongly typed index, that
     * is, @ref invalid_idx.
     *
     * A void index always boolean casts to `false`.
     */
    template<>
    struct idx<void> {
        /** @brief Returns <code>std::numeric_limits<weak_idx_t>::max()</code>.
         */
        inline operator weak_idx_t() const;

        /** @brief Returns `false`.
         */
        inline explicit operator bool() const;
    };


    /** @brief *The* invalid strongly typed index.
     *
     * Use this constant to refer to any invalid strongly typed index, e.g. `if (my_idx == he::invalid_idx)` or
     * `idx<node> a_node = invalid_idx`.
     */
    static const idx<void> invalid_idx{};


    /** @cond */
    template<class T, class U,
            class = typename std::enable_if<std::is_integral<U>::value>::type>
    inline bool operator==(U a, idx<T> b) {
        return a == weak_idx_t(b);
    }

    template<class T, class U,
            class = typename std::enable_if<std::is_integral<U>::value>::type>
    inline bool operator==(idx<T> b, U a) {
        return a == weak_idx_t(b);
    }

    template<class T, class U,
            class = typename std::enable_if<std::is_integral<U>::value>::type>
    inline bool operator!=(U a, idx<T> b) {
        return a != weak_idx_t(b);
    }

    template<class T, class U,
            class = typename std::enable_if<std::is_integral<U>::value>::type>
    inline bool operator!=(idx<T> b, U a) {
        return a != weak_idx_t(b);
    }

    template<class T, class U>
    inline bool operator==(idx<T> a, idx<U> b) {
        return weak_idx_t(a) == weak_idx_t(b);
    }

    template<class T, class U>
    inline bool operator!=(idx<T> a, idx<U> b) {
        return weak_idx_t(a) != weak_idx_t(b);
    }

    template<class T>
    inline std::ostream &operator<<(std::ostream &s, idx<T> i) {
        return s << "<" << weak_idx_t(i) << ">";
    }

    inline std::ostream &operator<<(std::ostream &s, idx<void>) {
        return s << "<-1>";
    }

    /** @endcond */


    template<class T>
    idx<T>::idx() noexcept : _i(std::numeric_limits<weak_idx_t>::max()) {}

    template<class T>
    idx<T>::idx(idx<void>) noexcept : idx() {}

    template<class T>
    idx<T>::operator weak_idx_t() const { return _i; }

    template<class T>
    idx<T>::operator bool() const {
        return *this != invalid_idx;
    }

    template<class T>
    template<class U, class>
    idx<T>::idx(idx<U> other) noexcept : _i(weak_idx_t(other)) {}

    template <class T>
    template<class U, class>
    idx<T>::operator idx<U>() const { return idx<U>(_i); }

    template<class T>
    idx<T> &idx<T>::operator=(idx<void>) {
        _i = std::numeric_limits<weak_idx_t>::max();
        return *this;
    }

    idx<void>::operator weak_idx_t() const {
        return std::numeric_limits<weak_idx_t>::max();
    }

    idx<void>::operator bool() const {
        return false;
    }

}

#endif //HE_IDX_H
