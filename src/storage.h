//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 11/08/16.
//
/** @file storage.h
 * @brief Simple implementations of index pool and attachable storages.
 */

#ifndef HE_STORAGE_H
#define HE_STORAGE_H

#include <cassert>
#include <limits>
#include <sstream>
#include <type_traits>
#include "storage_base.h"
#include "iterators.h"

/** namespace he */
namespace he {

    /** @brief Free-list based implementation of an index pool.
     *
     * This index pool implements O(1) queries to @ref is_allocated using a `std::vector<bool>` to flag the indices that
     * are allocated, and O(1) query to @ref get_allocation_idx by storing in a `std::vector<std::size_t>` all the freed
     * elements.
     *
     * The size in memory of this pool base is linear in the capacity of the pool.
     *
     * @see idx_pool_base
     */
    class idx_pool : public idx_pool_base {
        std::vector<std::size_t> _free;
        std::vector<bool> _chi;
    public:

        /** @brief Implementation of @ref idx_pool_base::is_allocated. Constant runtime.
         * @see idx_pool_base::is_allocated
         */
        inline bool is_allocated(std::size_t i) const override;

        /** @brief Implementation of @ref idx_pool_base::get_first_allocated_entry.
         * Slightly optimized by scanning the allocation vector.
         * @see idx_pool_base::get_first_allocated_entry
         */
        inline std::size_t get_first_allocated_entry(std::size_t begin) const override;

    protected:
        /**
         * Removes the element from the free list if necessary, and resized the allocation vector.
         * @see idx_pool_base::before_allocate
         */
        inline void before_allocate(std::size_t i) override;

        /**
         * Adds the element to the free list, and caches the fact that is not anymore allocated.
         * @see idx_pool_base::after_destroy
         */
        inline void after_destroy(std::size_t i) override;

        /**
         * Returns the first free element, if any, or the capacity.
         * @see idx_pool_base::get_allocation_idx
         */
        inline std::size_t get_allocation_idx() const override;
    };

    /** @cond */
    namespace impl {
        template<class T>
        using vector_storage_base = storage_base<
                typename std::vector<T>::value_type,
                typename std::vector<T>::reference,
                typename std::vector<T>::const_reference
        >;
    }
    /** @endcond* */

    /** @brief An implementation of an attachable storage based on `std::vector`.
     *
     * All the operations defined in this class wrap around `std::vector` methods. The (const) reference type is the one
     * defined by `std::vector<T>`. The gaps in the vector where elements are not allocated are filled with the default
     * value specified in @ref storage::storage.
     *
     * @tparam T type of the object in the storage.
     *
     * @note `T` must support copy assignment and copy construction.
     *
     * @see storage_base
     */
    template<class T>
    class storage : public impl::vector_storage_base<T> {
    public:
        using typename impl::vector_storage_base<T>::value_type;
        using typename impl::vector_storage_base<T>::reference;
        using typename impl::vector_storage_base<T>::const_reference;
        using impl::vector_storage_base<T>::pool;
        using impl::vector_storage_base<T>::is_attached;

        /** @brief An iterator that returns a reference to the allocated elements in this storage.
         */
        using iterator = map_item_iterator<storage<T>, chi_iterator>;

        /** @brief An iterator that returns a const reference to the allocated elements in this storage.
         */
        using const_iterator = map_item_iterator<const storage<T>, chi_iterator>;

        /** @brief Creates a new empty, unattached storage where the default value is a default constructed `T`.
         */
        template<class Q = value_type,
                class = typename std::enable_if<std::is_default_constructible<Q>::value>::type>
        inline storage();

        /** @brief Creates a new empty, unattached storage where the default value is @p default_value.
         *
         * @param default_value A copy of this object is made for later assignment.
         */
        inline storage(value_type const &default_value);

        /** @brief Implementation of @ref storage_base::operator[].
         * @see storage_base::operator[]
         */
        inline const_reference operator[](std::size_t i) const override;

        /** @brief Implementation of @ref storage_base::operator[].
         * @see storage_base::operator[]
         */
        inline reference operator[](std::size_t i) override;

        /** @brief Returns an iterator pointing to the first allocated element of this storage.
         */
        inline iterator begin();

        /** @brief Returns an iterator pointing to past-the last allocated element of this storage.
         */
        inline iterator end();

        /** @brief Returns a const iterator pointing to the first allocated element of this storage.
         */
        inline const_iterator begin() const;

        /** @brief Returns a const iterator pointing to past-the last allocated element of this storage.
         */
        inline const_iterator end() const;

        /** @brief Returns an iterator pointing to the first allocated element of this storage.
         */
        inline const_iterator cbegin() const;

        /** @brief Returns a const iterator pointing to past-the last allocated element of this storage.
         */
        inline const_iterator cend() const;

    protected:
        /**
         * Resizes the internal vector container to the capacity of the parent index pool.
         * @see storage_base::after_attach
         */
        inline void after_attach() override;

        /**
         * Clears the internal vector container.
         * @see storage_base::before_detach
         */
        inline void before_detach() override;

        /**
         * Makes sure there is a default element at position @p idx.
         * Calls either `std::vector::push_back` or `std::vector::resize`.
         * @see storage_base::allocate
         */
        inline void allocate(std::size_t idx) override;

        /**
         * Analoguous to @ref allocate, but performs afterwards an assignment from the object at position @p orig to the
         * element at position @p idx.
         * @see storage_base::clone
         */
        inline void clone(std::size_t idx, std::size_t orig) override;

        /**
         * Assigns the default value to element at position @p idx.
         * @see storage_base::destroy
         */
        inline void destroy(std::size_t idx) override;

    private:
        value_type _default;
        std::vector<value_type> _v;
    };

    bool idx_pool::is_allocated(std::size_t i) const {
        return i < _chi.size() ? _chi[i] : false;
    }

    inline std::size_t idx_pool::get_first_allocated_entry(std::size_t begin) const {
        if (begin < _chi.size()) {
            std::vector<bool>::const_iterator it = _chi.begin() + begin;
            while (it != _chi.end()) {
                if (*it) {
                    return std::size_t(it - _chi.begin());
                }
                ++it;
            }
        }
        return std::numeric_limits<std::size_t>::max();
    }

    void idx_pool::after_destroy(std::size_t i) {
        idx_pool_base::after_destroy(i);
        _chi[i] = false;
        _free.push_back(i);
    }

    void idx_pool::before_allocate(std::size_t i) {
        if (i == _chi.size()) {
            _chi.push_back(true);
        } else if (i < _chi.size()) {
            assert(i == _free.back());
            _free.pop_back();
            _chi[i] = true;
        } else {
            _chi.resize(i + 1, false);
            _chi[i] = true;
        }
        idx_pool_base::before_allocate(i);
    }

    std::size_t idx_pool::get_allocation_idx() const {
        return _free.empty() ? capacity() : _free.back();
    }

    template <class T>
    template<class Q, class>
    storage<T>::storage() : _default(), _v() {}

    /** @cond */
    /* Doxygen can't properly match these to their declaration because of the usage of :: or because of the override
     * keyword, therefore we just disable them.
     */

    template <class T>
    storage<T>::storage(storage<T>::value_type const &default_value) : _default(default_value), _v() {}

    template <class T>
    typename storage<T>::const_reference storage<T>::operator[](std::size_t i) const {
        return _v[i];
    }

    template <class T>
    typename storage<T>::reference storage<T>::operator[](std::size_t i) {
        return _v[i];
    }

    /** @endcond */

    template <class T>
    typename storage<T>::iterator storage<T>::begin() {
        assert(is_attached());
        return iterator(*this, pool()->begin());
    }


    template <class T>
    typename storage<T>::iterator storage<T>::end() {
        assert(is_attached());
        return iterator(*this, pool()->end());
    }


    template <class T>
    typename storage<T>::const_iterator storage<T>::begin() const {
        assert(is_attached());
        return const_iterator(*this, pool()->begin());
    }

    template <class T>
    typename storage<T>::const_iterator storage<T>::end() const {
        assert(is_attached());
        return const_iterator(*this, pool()->end());
    }

    template <class T>
    typename storage<T>::const_iterator storage<T>::cbegin() const {
        return begin();
    }

    template <class T>
    typename storage<T>::const_iterator storage<T>::cend() const {
        return end();
    }


    template <class T>
    void storage<T>::after_attach() {
        _v.resize(pool()->capacity(), _default);
    }

    template <class T>
    void storage<T>::before_detach() {
        _v.clear();
    }

    template <class T>
    void storage<T>::allocate(std::size_t idx) {
        if (idx == _v.size()) {
            _v.push_back(_default);
        } else if (idx > _v.size()) {
            _v.resize(idx + 1, _default);
        }
    }

    template <class T>
    void storage<T>::clone(std::size_t idx, std::size_t orig) {
        if (idx < _v.size()) {
            _v[idx] = _v[orig];
        } else if (idx == _v.size()) {
            _v.push_back(_v[orig]);
        } else {
            _v.resize(idx + 1, _default);
            _v[idx] = _v[orig];
        }
    }

    template <class T>
    void storage<T>::destroy(std::size_t idx) {
        _v[idx] = _default;
    }

}

#endif //HE_STORAGE_H
