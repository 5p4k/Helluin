//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 31/08/16.
//
/** @file graph_base.h
 * @brief Basic definitions of arcs, nodes, and graph.
 */

#ifndef HE_GRAPH_BASE_H
#define HE_GRAPH_BASE_H

#include "storage.h"
#include "idx.h"
#include "graph_decl.h"
#include <memory>
#include <array>
#include <functional>

/** @namespace he */
namespace he {

    /** @brief Defines the traits of a forward iterator with value type `std::pair<idx<arc>, arc_dir>`.
     *
     * Subclasses of @ref graph_base must implement this iterator to erase their own iterator type and
     * allow @ref graph_base to be operated with no need for specialized, templated algorithms.
     * @see pseudo_fwd_iterator.
     */
    using pseudo_adj_arcs_iterator = pseudo_fwd_iterator<std::pair<idx<arc>, arc_dir>>;

    /** @brief Wraps again a @ref pseudo_adj_arcs_iterator into a forward iterator of value
     *         type `std::pair<idx<arc>, arc_dir>`.
     * @see pseudo_fwd_iterator_wrapper.
     */
    using adj_arcs_iterator = pseudo_fwd_iterator_wrapper<std::pair<idx<arc>, arc_dir>>;

    class adj_nodes_iterator;

    /** @brief Main class that defines what a graph is.
     *
     * @warning This class does not actually store any information on the nodes and on the arcs; its sole purpose is to
     * manipulate @ref idx_pool_base "index pools" that refer to arcs and nodes. **Subclasses are responsible to
     * implement their own storage strategy for arcs and nodes**; see for example @ref adj_list_graph.
     *
     * This class defines the essential methods to manipulate a graph, i.e. @ref emplace_node "node insertion",
     * @ref emplace_arc "arc insertion", @ref erase "arc and node deletion", @ref endpoint "access to arc endpoints",
     * @ref adjacent_arcs "graph traversal".<br>
     * It also defines some handy functions, such as @ref any_node, @ref any_neighbor and
     * @ref is_adjacent "adjacency test", and @ref clone_node "node cloning" and @ref clone_arc "arc cloning".<br>
     * Access to nodes and arcs is index-based, strongly typed via `idx<arc>` and `idx<node>`. In case you need to
     * convert from a plain old numeric type to a strongly typed index, use @ref node_at and @ref arc_at.<br>
     * Access to the underlying index pools is available via @ref node_pool and @ref arc_pool. This is the preferred way
     * of storing per-arc or per-node data.
     *
     * The minimal set of functions that a subclass must implement is
     *
     *  - @ref endpoint `idx<node> endpoint(idx<arc> i, arc_endpt endpt) const`
     *  - @ref degree `weak_idx_t degree(idx<node> n, arc_dir dir = arc_dir::any) const`
     *  - @ref touch `void touch(idx<arc> i, idx<arc> clone_from, idx<node> from, idx<node> to)`
     *  - @ref enum_adj_arcs `std::unique_ptr<pseudo_adj_arcs_iterator> enum_adj_arcs(idx<node> n, arc_dir filter) const`
     *
     * Other functions that the subclasses can override, e.g. for performance reasons, to avoid redundant casts and
     * copies, are
     *
     *  - @ref endpoints `std::array<idx<node>, 2> endpoints(idx<arc> i) const`
     *  - @ref is_adjacent `bool is_adjacent(idx<node> n1, idx<node> n2, bool directed = true) const`
     *  - @ref any_adjacent_arc `inline idx<arc> any_adjacent_arc(idx<node> n, arc_dir dir = arc_dir::any) const`
     *  - @ref touch `void touch(idx<node> i, idx<node> cloned_from)`
     *  - @ref before_delete `void before_delete(idx<arc> i)`
     *  - @ref before_delete `void before_delete(idx<node> i)`
     *
     *  @note The reason for having a virtual class as a base for a graph, is to separate the actual graph storage
     *  (e.g. adjacency list, adjacency matrix) from the graph interface. Moreover, this allows to manipulate a graph
     *  without having to know the actual type of nodes or arcs. In this way one can implement an algorithm without
     *  having to template it for a specific graph type.
     */
    class graph_base {
        std::unique_ptr<idx_pool_base> _npool;
        std::unique_ptr<idx_pool_base> _apool;
    public:
        /** @brief Type attached to every node.
         *
         * Defaults to `void`. Subclasses can override this and define a custom, privileged type which is going to be
         * attached to every node.
         * @see store_nodes
         * @see graph_traits
         */
        using node_type = void;

        /** @brief Type of storage that holds the node type.
         *
         * For a `void` node type, this is as well `void`.
         * @see store_nodes
         * @see graph_traits
         */
        using node_storage_type = void;

        using node_reference = void;        ///< @brief Reference to a @ref node_type.
        using node_const_reference = void;  ///< @brief Const reference to a @ref node_type.
        using node_pointer = void;          ///< @brief Pointer to a @ref node_type.
        using node_const_pointer = void;    ///< @brief Const pointer to a @ref node_type.

        /** @brief Type attached to every arc.
         *
         * Defaults to `void`. Subclasses can override this and define a custom, privileged type which is going to be
         * attached to every arc.
         * @see store_arcs
         * @see graph_traits
         */
        using arc_type = void;

        /** @brief Type of storage that holds the arc type.
         *
         * For a `void` arc type, this is as well `void`.
         * @see store_arcs
         * @see graph_traits
         */
        using arc_storage_type = void;

        using arc_reference = void;         ///< @brief Reference to a @ref node_type.
        using arc_const_reference = void;   ///< @brief Const reference to a @ref node_type.
        using arc_pointer = void;           ///< @brief Pointer to a @ref node_type.
        using arc_const_pointer = void;     ///< @brief Const pointer to a @ref node_type.

        /** @brief Range-iterable proxy object holding two iterators to the arcs adjacent to a node.
         * @see enum_adj_arcs
         * @see adjacent_arcs
         */
        using adj_arcs = simple_range<adj_arcs_iterator>;

        /** @brief Range-iterable proxy object holding two iterators to the nodes adjacent to a node.
         * @see enum_adj_arcs
         * @see adjacent_arcs
         * @see adjacent_nodes
         */
        using adj_nodes = simple_range<adj_nodes_iterator>;

        /** @brief Creates a new empty graph.
         *
         * The internal arc and node pools are created, empty, as instances of @ref idx_pool. It is possible to then
         * obtain ownership of the pools by calling @ref release.
         */
        inline graph_base();

        /** @brief Creates a new graph loading external pool bases.
         *
         * To allow the caller to customize the used index pool, this overload can take external pools that will be used
         * for arcs and nodes. It is possible then to obtain again ownership of the pools by calling @ref release.
         * @param nodes the new pool for nodes.
         * @param arcs the new pool for arcs.
         */
        inline graph_base(std::unique_ptr<idx_pool_base> &&nodes, std::unique_ptr<idx_pool_base> &&arcs);

        virtual ~graph_base() = default;

        /** @{ */

        /** @brief Returns a constant reference to the underlying arc pool.
         */
        inline idx_pool_base const &arc_pool() const;

        /** @brief Returns a constant reference to the underlying node pool.
         */
        inline idx_pool_base const &node_pool() const;

        /** @brief Releases ownership of the internal node and arc pools, making this instance unusable.
         *
         * This can be used to keep external data alive beyond the scope of @ref graph_base.
         * @returns A pair of unique pointers, where the first corresponds to the node storage, and the second to arcs.
         * @warning Any further call to a method of graph_base may dereference a null unique pointer to the node and arc
         * pool, hence any usage of the graph after release is undefined behavior.
         */
        inline std::pair<std::unique_ptr<idx_pool_base>, std::unique_ptr<idx_pool_base>> release();

        /** @brief Converts a numeric index into a strongly typed node index.
         * @returns A strongly typed `idx<node>` containing @p index if @p index is allocated, otherwise `invalid_idx`.
         */
        inline idx<node> node_at(weak_idx_t index) const;

        /** @brief Converts a numeric index into a strongly typed arc index.
         *
         * @returns A strongly typed `idx<arc>` containing @p index if @p index is allocated, otherwise `invalid_idx`.
         */
        inline idx<arc> arc_at(weak_idx_t index) const;

        /** @} */

        /** @{ */

        /** @brief Allocates a new node.
         * @returns The index of the newly allocated node.
         */
        inline idx<node> emplace_node();

        /** @brief Allocates a new arc with the given endpoints.
         * @param tail the tail of the arc.
         * @param head the head of the arc.
         * @returns the index of the newly created arc.
         */
        inline idx<arc> emplace_arc(idx<node> tail, idx<node> head);

        /** @brief Clones a node and optionally all the arcs incident to it.
         *
         * This is different than just emplacing a new node because it invokes @ref idx_pool_base::clone, hence
         * effectively cloning all the data attached to @p other via the index pool.
         * @param other node to clone.
         * @param clone_delta if `true`, all the arcs incident to @p other will be cloned, with the newly cloned
         * endpoint as endpoint.
         * @returns the index of the newly cloned node.
         */
        idx<node> clone_node(idx<node> other, bool clone_delta);

        /** @brief Clones an arc. Optionally changes one endpoint.
         *
         * This is different than just emplacing a new arc because it invokes @ref idx_pool_base::clone, hence
         * effectively cloning all the data attached to @p other via the index pool.
         * @param other arc to clone.
         * @param change_tail the new tail for the cloned arc. Specifying `invalid_idx` to clone the same tail
         * of @p other.
         * @param change_head the new head for the cloned arc. Specifying `invalid_idx` to clone the same head
         * of @p other.
         * @return the index of the newly cloned arc.
         */
        inline idx<arc> clone_arc(idx<arc> other,
                                  idx<node> change_tail = invalid_idx, idx<node> change_head = invalid_idx);

        /** @brief Changes an arc's endpoints.
         * @param i the arc for which to change the endpoints.
         * @param new_tail the new tail node. Specifying `invalid_idx` causes no change in the tail of the arc.
         * @param new_head the new head node. Specifying `invalid_idx` causes no change in the head of the arc.
         */
        inline void change_arc(idx<arc> i, idx<node> new_tail = invalid_idx, idx<node> new_head = invalid_idx);

        /** @brief Deletes a node and all the arcs adjacent to it.
         *
         * First deletes all the adjacent arcs, and then deletes the node.
         * @param i node to delete.
         */
        void erase(idx<node> i);

        /** @brief Deletes an arc.
         * @param i the arc to delete.
         */
        void erase(idx<arc> i);

        /** @} */

        /** @{ */

        /** @brief Extracts an arc's endpoints.
         *
         * The default implementation calls twice @ref endpoint using @p i as argument. Subclasses can override this
         * method to provide a faster lookup.
         * @param i the index of the arc for which to retrieve the endpoints.
         * @returns the endpoints of @p i. The tail is the first and the head the second index in the pair.
         * @todo This method is not very useful, in most cases one knows which endpoint one wants. Consider deleting it.
         */
        inline virtual std::pair<idx<node>, idx<node>> endpoints(idx<arc> i) const;

        /** @brief Returns a given endpoint of an arc.
         *
         * Subclasses must implement this function to allow access to an arc content.
         * @param i index of an existing arc.
         * @param endpt the endpoint to select.
         * @returns The tail or the head of @p i according to @p endpt.
         */
        virtual idx<node> endpoint(idx<arc> i, arc_endpt endpt) const = 0;

        /** @brief Returns the other endpoint.
         *
         * The only supported for @p endpt are @ref arc_dir::incoming, @ref arc_dir::outgoing and @ref arc_dir::loop. It
         * is considered an error to call this method with any other value for it.
         *
         * @note The idea behind this method is: if I have an arc and its direction, it means it is referred to a
         * vertex, and I know which vertex it is referred to. Therefore I may be interested only in the other, unknown,
         * endpoint.
         *
         * @param i index of an existing arc.
         * @param dir the direction of @p i w.r.t. one of its endpoints.
         * @returns the tail of @p i if @p dir is @ref arc_dir::incoming or @ref arc_dir::loop, and the head of @p i if
         * @p dir is instead @ref arc_dir::outgoing.
         * @throws std::invalid_argument if @p dir is not one of `incoming`, `loop`, or `outgoing`.
         */
        inline idx<node> endpoint(idx<arc> i, arc_dir dir) const;

        /** @brief Alias for @ref endpoint. All the remarks for the latter apply.
         */
        inline idx<node> endpoint(std::pair<idx<arc>, arc_dir> other) const;

        /** @brief Performs (un)directed adjacency test between two nodes.
         *
         * Returns `true` if and only if there is an arc from @p n1 to @p n2 or, if @p directed is set false, if there
         * is an arc from @p n2 to @p n1 as well. If @p n1 and @p n2 are the same, this method will return true if there
         * is at least one loop adjacent to @p n1.
         *
         * @note The default implementation calls several times @ref degree. If `n1 == n2`, a call to
         * `degree(n1, arc_dir::loop)` is sufficient to answer the query. For the other cases, the smaller neighborhood
         * is scanned arc by arc (therefore the method will call @ref degree and then enumerate the arcs adjacent to
         * one of the two nodes).
         *
         * @param n1 first node to test.
         * @param n2 second node to test.
         * @param directed if true, a the method searches for a `(n1, n2)` directed arc; otherwise any arc between @p n1
         * and @p n2 causes the method to return true.
         * @return True if and only if there is an arc between @p n1 and @p n2, possibly a directed arc depending
         * on @p directed.
         */
        virtual bool is_adjacent(idx<node> n1, idx<node> n2, bool directed = true) const;

        /** @brief Returns the number of arcs matching @p dir adjacent to @p n.
         *
         * Subclasses must implement this method. All the values in the @ref arc_dir enum must be implemented.
         *
         * @note Ideally, this function's runtime should be _O(1)_. The degree is used (e.g. in @ref is_adjacent) to
         * precondition the algorithm used.
         *
         * @param n node for which the degree is queried.
         * @param dir a set of flags denoting which types of arc should be counted.
         * @returns the number of distinct arcs adjacent to @p n matching @p dir.
         */
        virtual weak_idx_t degree(idx<node> n, arc_dir dir = arc_dir::any) const = 0;

        /** @brief Returns an iterable range containing all the arcs adjacent to @p n matching @p dir.
         *
         * @note The validity of the iterators of this range object is determined by the validity of the
         * object @ref pseudo_adj_arcs_iterator returned by the subclass at @ref enum_adj_arcs.
         *
         * @param n node of which the arcs are enumerated.
         * @param dir only arcs matching this direction will be enumerated.
         * @returns A range enumerable proxy object containing a begin and past-the-end iterator.
         */
        inline adj_arcs adjacent_arcs(idx<node> n, arc_dir dir = arc_dir::any) const;

        /** @brief A wrapper of @ref endpoint around the elements of @ref adjacent_arcs. All the properties of the
         *         latter apply.
         */
        inline adj_nodes adjacent_nodes(idx<node> n, arc_dir dir = arc_dir::any) const;

        /** @brief Extracts an arbitrary arc from those adjacent to @p n.
         *
         * This shorthand method can be useful when traversing directed trees, to obtain a parent or a child, when it is
         * known to be unique.
         *
         * @note The current implementation checks if the @ref degree of @p n in direction @p dir is greater than zero;
         * if so, it returns the first element in the enumeration produced by @ref enum_adj_arcs, otherwise returns
         * `invalid_idx`. Subclasses can supply a better implementation.
         *
         * @param n node whose adjacent arcs are tested.
         * @param dir the returned arc must match this direction.
         * @return the index of an arc matching @p dir adjacent to @p n if it exists, `invalid_idx` otherwise.
         */
        virtual inline idx<arc> any_adjacent_arc(idx<node> n, arc_dir dir = arc_dir::any) const;

        /** @brief A wrapper of @ref endpoint around @ref any_adjacent_arc. All the properties of the latter apply.
         * @returns the index of the node on the other side of an arc matching @p dir adjacent to @p n, if such an arc
         * exists, otherwise `invalid_idx`.
         */
        inline idx<node> any_neighbor(idx<node> n, arc_dir dir = arc_dir::any) const;

        /** @brief Returns the strongly typed index to an arbitrary node, if any is allocated, otherwise `invalid_idx`.
         */
        inline idx<node> any_node() const;

        /** @brief Returns the strongly typed index to an arbitrary arc, if any is allocated, otherwise `invalid_idx`.
         */
        inline idx<arc> any_arc() const;

        /** @} */

    protected:

        /** @brief Allocates (or clones) the actual stored data for a node, if necessary.
         *
         * The default implementation does nothing. Subclasses can use this method to initialize per-node storage, e.g.
         * an adjacency list. This method is guaranteed to be called before @ref emplace_node and @ref clone_node exit.
         *
         * @note If the subclasses store per-node data as attached storages to the node pool, and such per-node data is
         * default constructible, there is no need to override this method. This should be used only for late
         * initialization.
         *
         * @param i index that was allocated for the new node.
         * @param cloned_from if the node was cloned from an existing one, this contains the original index. For a
         * newly allocated node, this is `invalid_idx`.
         */
        inline virtual void touch(idx<node> i, idx<node> cloned_from);

        /** @brief Allocates, clones, or edits the actual stored data for an arc, if necessary.
         *
         * Subclasses are required to implement this function in order to store the actual graph structure. This method
         * is guaranteed to be called before @ref emplace_arc, @ref clone_arc and @ref change_arc exit. This is the
         * unique method that performs late initialization for a newly created or cloned arc, or changes the nodes
         * adjacent to a preexisting arc.
         *
         * @param i index of the already allocated arc.
         * @param clone_from if the arc is being cloned from an existing one, this contains the original arc. In all
         * other cases (also for @ref change_arc called on a previously cloned node), this is `invalid_idx`.
         * @param from index of an allocated node that corresponds to the tail of the arc. If the arc is begin cloned or
         * edited, this can be `invalid_idx`, meaning that the same tail of @p i or @p clone_from must be taken. If
         * differs from `invalid_idx`, then @p from must override the tail of @p i or @p clone_from.
         * @param to index of an allocated node that corresponds to the head of the arc. If the arc is begin cloned or
         * edited, this can be `invalid_idx`, meaning that the same head of @p i or @p clone_from must be taken. If
         * differs from `invalid_idx`, then @p from must override the head of @p i or @p clone_from.
         */
        virtual void touch(idx<arc> i, idx<arc> clone_from, idx<node> from, idx<node> to) = 0;

        /** @brief Callback before @p i is destroyed in the index pool.
         *
         * Subclasses can use this to tear down arc-specific data. The default implementation does nothing.
         * @param i arc to be destroyed.
         */
        inline virtual void before_delete(idx<arc> i);

        /** @brief Callback before @p i is destroyed in the index pool.
         *
         * Subclasses can use this to tear down arc-specific data. The default implementation does nothing.
         * @param i node to be destroyed.
         */
        inline virtual void before_delete(idx<node> i);

        /** @brief Returns a pseudo iterator object that enumerates the arcs adjacent to @p n.
         *
         * Subclasses must implement this method and return their own subclass of @ref pseudo_adj_arcs_iterator.
         * @param n node whose adjacent arcs are to be enumerated.
         * @param filter only arcs matching this filter should be enumerated by the returned value.
         * @return a unique instance of a @ref pseudo_adj_arcs_iterator.
         */
        virtual std::unique_ptr<pseudo_adj_arcs_iterator> enum_adj_arcs(idx<node> n, arc_dir filter) const = 0;
    };

    /** @brief Wraps @ref graph_base::endpoint around an @ref adj_arcs_iterator; behaves like the latter.
     * @see target_remap_iterator_base, adj_arcs_iterator, graph_base::endpoint.
     */
    class adj_nodes_iterator
            : public target_remap_iterator_base<adj_nodes_iterator, adj_arcs_iterator, const graph_base> {
        using base_iterator = target_remap_iterator_base<adj_nodes_iterator, adj_arcs_iterator, const graph_base>;
    public:
        /** @cond */
        using base_iterator::base_iterator;
        using base_iterator::inner;
        using base_iterator::target;
        using value_type = idx<node>;
        using pointer = void;
        using reference = idx<node>;

        inline idx<node> operator*() {
            return target().endpoint(*inner());
        }
        /** @endcond */
    };

    graph_base::graph_base() : _npool(new idx_pool()), _apool(new idx_pool()) {}

    graph_base::graph_base(std::unique_ptr<idx_pool_base> &&nodes, std::unique_ptr<idx_pool_base> &&arcs)
            : _npool(std::move(nodes)), _apool(std::move(arcs)) {}

    void graph_base::touch(idx<node>, idx<node>) {}

    void graph_base::before_delete(idx<arc>) {}

    void graph_base::before_delete(idx<node>) {}

    void graph_base::change_arc(idx<arc> i, idx<node> new_tail, idx<node> new_head) {
        assert(_apool->is_allocated(i));
        assert(not new_tail or _npool->is_allocated(new_tail));
        assert(not new_head or _npool->is_allocated(new_head));
        touch(i, invalid_idx, new_tail, new_head);
    }

    idx<node> graph_base::node_at(weak_idx_t index) const {
        return _npool->is_allocated(index) ? idx<node>(index) : invalid_idx;
    }

    idx<arc> graph_base::arc_at(weak_idx_t index) const {
        return _apool->is_allocated(index) ? idx<arc>(index) : invalid_idx;
    }

    idx<node> graph_base::endpoint(idx<arc> i, arc_dir dir) const {
        switch (dir) {
            case arc_dir::outgoing:
                return endpoint(i, arc_endpt::head);
            case arc_dir::loop:
            case arc_dir::incoming:
                return endpoint(i, arc_endpt::tail);
            default:
                throw std::invalid_argument("An arc has a mixed direction.");
        }
    }

    idx<node> graph_base::endpoint(std::pair<idx<arc>, arc_dir> other) const {
        return endpoint(other.first, other.second);
    }

    std::pair<idx<node>, idx<node>> graph_base::endpoints(idx<arc> i) const {
        return {endpoint(i, arc_endpt::tail), endpoint(i, arc_endpt::head)};
    }

    idx<node> graph_base::any_node() const {
        if (_npool->size() > 0) {
            return {saturate_cast<weak_idx_t>(*_npool->begin())};
        }
        return invalid_idx;
    }

    idx<arc> graph_base::any_arc() const {
        if (_apool->size() > 0) {
            return {saturate_cast<weak_idx_t>(*_apool->begin())};
        }
        return invalid_idx;
    }

    graph_base::adj_arcs graph_base::adjacent_arcs(idx<node> n, arc_dir dir) const {
        assert(_npool->is_allocated(n));
        return adj_arcs(adj_arcs_iterator(enum_adj_arcs(n, dir)), adj_arcs_iterator());
    }

    graph_base::adj_nodes graph_base::adjacent_nodes(idx<node> n, arc_dir dir) const {
        assert(_npool->is_allocated(n));
        return adj_nodes(adj_nodes_iterator(*this, adj_arcs_iterator(enum_adj_arcs(n, dir))),
                         adj_nodes_iterator(*this, adj_arcs_iterator()));
    }

    idx<arc> graph_base::any_adjacent_arc(idx<node> n, arc_dir dir) const {
        assert(_npool->is_allocated(n));
        if (degree(n, dir) == 0) {
            return invalid_idx;
        }
        return enum_adj_arcs(n, dir)->dereference().first;
    }

    idx<node> graph_base::any_neighbor(idx<node> n, arc_dir dir) const {
        const idx<arc> a = any_adjacent_arc(n, dir);
        if (not a) {
            return invalid_idx;
        }
        switch (dir) {
            case arc_dir::any:
            case arc_dir::any_no_loop:
            case arc_dir::incoming:
            case arc_dir::any_incoming:
                return endpoint(a, arc_dir::incoming);
            case arc_dir::outgoing:
            case arc_dir::any_outgoing:
                return endpoint(a, arc_dir::outgoing);
            case arc_dir::loop:
                return n;
        }
    }

    idx<arc> graph_base::clone_arc(idx<arc> other, idx<node> change_tail, idx<node> change_head) {
        assert(not change_tail or _npool->is_allocated(change_tail));
        assert(not change_head or _npool->is_allocated(change_head));
        // No need for checking for allocation, the pool will do it for us.
        idx<arc> new_arc = saturate_cast<weak_idx_t>(_apool->clone(other));
        touch(new_arc, other, change_tail, change_head);
        return new_arc;
    }

    idx<node> graph_base::emplace_node() {
        idx<node> new_node(saturate_cast<weak_idx_t>(_npool->allocate()));
        touch(new_node, invalid_idx);
        return new_node;
    }

    idx<arc> graph_base::emplace_arc(idx<node> tail, idx<node> head) {
        assert(_npool->is_allocated(tail));
        assert(_npool->is_allocated(head));
        idx<arc> new_arc(saturate_cast<weak_idx_t>(_apool->allocate()));
        touch(new_arc, invalid_idx, tail, head);
        return new_arc;
    }

    idx_pool_base const &graph_base::arc_pool() const {
        return *_apool;
    }

    idx_pool_base const &graph_base::node_pool() const {
        return *_npool;
    }

    std::pair<std::unique_ptr<idx_pool_base>, std::unique_ptr<idx_pool_base>> graph_base::release() {
        std::pair<std::unique_ptr<idx_pool_base>, std::unique_ptr<idx_pool_base>> retval;
        _npool.swap(retval.first);
        _apool.swap(retval.second);
        return retval;
    }

}

#endif //HE_GRAPH_BASE_H
