//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 04/09/16.
//

#ifndef HE_GRAPH_PARTS_H
#define HE_GRAPH_PARTS_H

#include "graph_base.h"
#include "storage.h"

namespace he {

    template<class N, class NStorage = storage<N>>
    class store_nodes : public virtual graph_base {
    public:
        using node_type = typename std::remove_reference<N>::type;
        using node_storage_type = typename std::enable_if<std::is_base_of<storage_base<node_type>, NStorage>::value, NStorage>::type;
        using node_const_reference = typename node_storage_type::const_reference;
        using node_reference = typename node_storage_type::reference;
        using node_pointer = typename address_of_type<node_reference>::type;
        using node_const_pointer = typename address_of_type<node_const_reference>::type;

        class iterator : public target_remap_iterator_base<iterator, chi_iterator, store_nodes> {
            using base_type = target_remap_iterator_base<iterator, chi_iterator, store_nodes>;
        public:
            using base_type::inner;
            using base_type::target;
            using base_type::base_type;

            inline std::pair<idx<node>, node_reference> operator*() {
                idx<node> i = target().node_at(*inner());
                return {i, target()[i]};
            }
        };

        class const_iterator : public target_remap_iterator_base<const_iterator, chi_iterator, store_nodes> {
            using base_type = target_remap_iterator_base<const_iterator, chi_iterator, store_nodes>;
        public:
            using base_type::inner;
            using base_type::target;
            using base_type::base_type;

            inline std::pair<idx<node>, node_const_reference> operator*() const {
                idx<node> i = target().node_at(*inner());
                return {i, target()[i]};
            }
        };

        inline store_nodes() {
            _nstorage.attach_to(graph_base::node_pool());
        }

        inline node_reference operator[](idx<node> i) {
            return _nstorage[i];
        }

        inline node_const_reference operator[](idx<node> i) const {
            return _nstorage[i];
        }

        template<class ...Args>
        inline idx<node> emplace_node(Args &&...args) {
            idx<node> i = graph_base::emplace_node();
            std::swap(_nstorage[i], node_type(std::forward<Args>(args)...));
            return i;
        }

        inline simple_range<iterator> nodes() {
            return simple_range<iterator>(iterator(*this, node_pool().begin()), iterator(*this, node_pool().end()));
        }

        inline simple_range<const_iterator> nodes() const {
            return simple_range<const_iterator>(const_iterator(*this, node_pool().begin()),
                                                iterator(*this, node_pool().end()));
        }

    private:
        node_storage_type _nstorage;
    };

    template<class A, class AStorage = storage<A>>
    class store_arcs : public virtual graph_base {
    public:
        using arc_type = typename std::remove_reference<A>::type;
        using arc_storage_type = typename std::enable_if<std::is_base_of<storage_base<arc_type>, AStorage>::value, AStorage>::type;
        using arc_const_reference = typename arc_storage_type::const_reference;
        using arc_reference = typename arc_storage_type::reference;
        using arc_pointer = typename address_of_type<arc_reference>::type;
        using arc_const_pointer = typename address_of_type<arc_const_reference>::type;

        class iterator : public target_remap_iterator_base<iterator, chi_iterator, store_arcs> {
            using base_type = target_remap_iterator_base<iterator, chi_iterator, store_arcs>;
        public:
            using base_type::inner;
            using base_type::target;
            using base_type::base_type;

            inline std::pair<idx<arc>, arc_reference> operator*() {
                idx<arc> i = target().arc_at(*inner());
                return {i, target()[i]};
            }
        };

        class const_iterator : public target_remap_iterator_base<const_iterator, chi_iterator, store_arcs> {
            using base_type = target_remap_iterator_base<const_iterator, chi_iterator, store_arcs>;
        public:
            using base_type::inner;
            using base_type::target;
            using base_type::base_type;

            inline std::pair<idx<arc>, arc_const_reference> operator*() const {
                idx<arc> i = target().arc_at(*inner());
                return {i, target()[i]};
            }
        };

        inline store_arcs() {
            _astorage.attach_to(graph_base::arc_pool());
        }

        inline arc_reference operator[](idx<arc> i) {
            return _astorage[i];
        }

        inline arc_const_reference operator[](idx<arc> i) const {
            return _astorage[i];
        }

        template<class ...Args>
        inline idx<arc> emplace_arc(idx<node> tail, idx<node> head, Args &&...args) {
            idx<arc> i = graph_base::emplace_arc(tail, head);
            std::swap(_astorage[i], arc_type(std::forward<Args>(args)...));
            return i;
        }

        inline simple_range<iterator> arcs() {
            return simple_range<iterator>(iterator(*this, arc_pool().begin()), iterator(*this, arc_pool().end()));
        }

        inline simple_range<const_iterator> arcs() const {
            return simple_range<const_iterator>(const_iterator(*this, arc_pool().begin()),
                                                iterator(*this, arc_pool().end()));
        }

    private:
        arc_storage_type _astorage;
    };

    namespace impl {
        template<class T, bool>
        struct default_storage {
            using type = storage<T>;
        };

        template<class T>
        struct default_storage<T, true> {
            using type = void;
        };
    }

    template<class Impl, class Node, class Arc,
            class NodeStorage = typename impl::default_storage<Node, std::is_void<Node>::value>::type,
            class ArcStorage = typename impl::default_storage<Arc, std::is_void<Arc>::value>::type>
    class graph : public std::enable_if<std::is_base_of<graph_base, Impl>::value, Impl>::type,
                  public store_nodes<Node, NodeStorage>,
                  public store_arcs<Arc, ArcStorage> {
    public:
        // Solve some ambiguities. Use the same cctors as graph_base
        using store_nodes<Node, NodeStorage>::operator[];
        using store_arcs<Arc, ArcStorage>::operator[];
        using Impl::Impl;
    };

    template<class Impl, class Node, class NodeStorage>
    class graph<Impl, Node, void, NodeStorage, void> :
            public std::enable_if<std::is_base_of<graph_base, Impl>::value, Impl>::type,
            public store_nodes<Node, NodeStorage> {
    public:
        using Impl::Impl;
    };

    template<class Impl, class Arc, class ArcStorage>
    class graph<Impl, void, Arc, void, ArcStorage> :
            public std::enable_if<std::is_base_of<graph_base, Impl>::value, Impl>::type,
            public store_nodes<Arc, ArcStorage> {
    public:
        using Impl::Impl;
    };

    template<class Impl>
    class graph<Impl, void, void, void, void> :
            public std::enable_if<std::is_base_of<graph_base, Impl>::value, Impl>::type {
    public:
        using Impl::Impl;
    };

}

#endif //HE_GRAPH_PARTS_H
