//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 31/08/16.
//

#include "graph_base.h"
#include <algorithm>

using namespace he;

void graph_base::erase(idx<node> i) {
    assert(_npool->is_allocated(i));
    // Delete all the incident arcs
    for (idx<arc> to_erase = any_adjacent_arc(i); to_erase; to_erase = any_adjacent_arc(i)) {
        erase(to_erase);
    }
    before_delete(i);
    _npool->destroy(i);
}

void graph_base::erase(idx<arc> i) {
    assert(_apool->is_allocated(i));
    before_delete(i);
    _apool->destroy(i);
}

idx<node> graph_base::clone_node(idx<node> other, bool clone_delta) {
    idx<node> new_node = saturate_cast<weak_idx_t>(_npool->clone(other));
    touch(new_node, other);
    if (clone_delta) {
        // Clone also the arcs
        for (auto const &pair : adjacent_arcs(other, arc_dir::any)) {
            switch (pair.second) {
                case arc_dir::incoming:
                    clone_arc(pair.first, invalid_idx, new_node);
                    break;
                case arc_dir::outgoing:
                    clone_arc(pair.first, new_node, invalid_idx);
                    break;
                case arc_dir::loop:
                    clone_arc(pair.first, new_node, new_node);
                    break;
                default:
                    throw std::invalid_argument("An arc has a mixed direction.");
            }
        }
    }
    return new_node;
}

bool graph_base::is_adjacent(idx<node> n1, idx<node> n2, bool directed) const {
    if (not node_pool().is_allocated(n1) or not node_pool().is_allocated(n2)) {
        return false;
    } else if (n1 == n2) {
        return degree(n1, arc_dir::loop) > 0;
    }
    arc_dir dir = arc_dir::any_no_loop;
    idx<node> scan = n2;
    idx<node> other = n1;
    // Scan the most convenient node.
    const bool scan_n1 = directed ? (degree(n1, arc_dir::outgoing) <= degree(n2, arc_dir::incoming))
                                  : (degree(n1, arc_dir::any_no_loop) <= degree(n2, arc_dir::any_no_loop));
    if (scan_n1) {
        // scan will contain n1, other n2
        std::swap(scan, other);
    }
    if (directed) {
        // Scan any, unless is directed. Then it depends on which node we have chosen.
        dir = scan_n1 ? arc_dir::outgoing : arc_dir::incoming;
    }
    // Now run the for loop
    auto gamma = adjacent_nodes(scan, dir);
    return std::find(gamma.begin(), gamma.end(), other) != gamma.end();
}