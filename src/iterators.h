//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 11/08/16.
//

#ifndef HE_ITERATORS_H
#define HE_ITERATORS_H

#include <iterator>
#include <memory>
#include "type_traits.h"

namespace he {

    template<class Subclass, class It>
    class remap_iterator_base {
    public:
        using outer_iterator = Subclass;
        using inner_iterator = typename std::enable_if<std::is_copy_constructible<It>::value, It>::type;
        using difference_type = typename std::iterator_traits<inner_iterator>::difference_type;
        using iterator_category = typename std::iterator_traits<inner_iterator>::iterator_category;

        inline remap_iterator_base(inner_iterator const &i) : _i(i) {}

        template <class Q = inner_iterator, class = typename std::enable_if<std::is_move_constructible<It>::value>::type>
        inline remap_iterator_base(inner_iterator &&i) : _i(std::move(i)) {}

        template<class Q = inner_iterator,
                class = typename std::enable_if<std::is_default_constructible<Q>::value>::type>
        inline remap_iterator_base() : _i() {}

        template<class Tubclass, class Jt,
                class = typename std::enable_if<std::is_convertible<Jt, inner_iterator>::value>::type>
        inline remap_iterator_base(remap_iterator_base<Tubclass, Jt> const &rib)
                : _i(rib._i) {}

        inline outer_iterator &operator++() {
            ++_i;
            return *static_cast<outer_iterator *>(this);
        }

        inline outer_iterator operator++(int) {
            outer_iterator copy(*static_cast<outer_iterator *>(this));
            ++(*this);
            return copy;
        }

        template<class Q = inner_iterator,
                class = typename std::enable_if<is_decrementable<Q>::value>::type>
        inline outer_iterator &operator--() {
            --_i;
            return *static_cast<outer_iterator *>(this);
        }

        template<class Q = inner_iterator,
                class = typename std::enable_if<is_decrementable<Q>::value>::type>
        inline outer_iterator operator--(int) {
            outer_iterator copy(*static_cast<outer_iterator *>(this));
            --_i;
            return copy;
        }

        template<class Tubclass, class Jt,
                class = typename std::enable_if<is_differentiable_as<inner_iterator, Jt, difference_type>::value>::type>
        inline difference_type operator-(remap_iterator_base<Tubclass, Jt> const &other) const {
            return _i - other._i;
        }

        template<class Jt,
                class = typename std::enable_if<is_differentiable_as<inner_iterator, Jt, difference_type>::value>::type>
        inline difference_type operator-(Jt const &other) const {
            return _i - other;
        }

        template<class Q = inner_iterator,
                class = typename std::enable_if<is_n_incrementable<Q, difference_type>::value>::type>
        inline outer_iterator &operator+=(difference_type n) {
            _i += n;
            return *static_cast<outer_iterator *>(this);
        }

        template<class Q = inner_iterator,
                class = typename std::enable_if<is_n_decrementable<Q, difference_type>::value>::type>
        inline outer_iterator &operator-=(difference_type n) {
            _i -= n;
            return *static_cast<outer_iterator *>(this);
        }

        template<class Tubclass, class Jt,
                class = typename std::enable_if<is_equatable<inner_iterator, Jt>::value>::type>
        inline bool operator==(remap_iterator_base<Tubclass, Jt> const &other) const {
            return _i == other._i;
        }

        template<class Tubclass, class Jt,
                class = typename std::enable_if<is_inequatable<inner_iterator, Jt>::value>::type>
        inline bool operator!=(remap_iterator_base<Tubclass, Jt> const &other) const {
            return _i != other._i;
        }

        template<class Jt,
                class = typename std::enable_if<is_equatable<inner_iterator, Jt>::value>::type>
        inline bool operator==(Jt const &other) const {
            return _i == other;
        }

        template<class Jt,
                class = typename std::enable_if<is_inequatable<inner_iterator, Jt>::value>::type>
        inline bool operator!=(Jt const &other) const {
            return _i != other;
        }

        inline inner_iterator const &inner() const {
            return _i;
        }

        inline inner_iterator &inner() {
            return _i;
        }

    private:
        template<class Tubclass, class Jt>
        friend
        class remap_iterator_base;

        inner_iterator _i;
    };

    template<class Subclass, class It, class Target>
    class target_remap_iterator_base : public remap_iterator_base<Subclass, It> {
    public:
        using target_type = Target;
        using typename remap_iterator_base<Subclass, It>::inner_iterator;

        inline target_type const &target() const {
            return *_target;
        }

        inline target_type &target() {
            return *_target;
        }

        inline target_remap_iterator_base(inner_iterator const &i)
                : remap_iterator_base<Subclass, It>(i), _target(nullptr) {}

        inline target_remap_iterator_base(target_type &target, inner_iterator const &i)
                : remap_iterator_base<Subclass, It>(i), _target(&target) {}

        template<class Tubclass, class Jt,
                class = typename std::enable_if<std::is_convertible<
                        remap_iterator_base<Tubclass, Jt>, remap_iterator_base<Subclass, It>>::value>::type>
        inline target_remap_iterator_base(target_type &target, remap_iterator_base<Tubclass, Jt> const &rib)
                : remap_iterator_base<Subclass, It>(rib), _target(&target) {}

        template<class Q = remap_iterator_base<Subclass, It>,
                class = typename std::enable_if<std::is_default_constructible<Q>::value>::type>
        inline target_remap_iterator_base()
                : remap_iterator_base<Subclass, It>(), _target(nullptr) {}

        template<class Q = remap_iterator_base<Subclass, It>,
                class = typename std::enable_if<std::is_default_constructible<Q>::value>::type>
        inline target_remap_iterator_base(target_type &target)
                : remap_iterator_base<Subclass, It>(), _target(&target) {}

        template<class Tubclass, class Jt, class Uarget,
                class = typename std::enable_if<std::is_convertible<
                        remap_iterator_base<Tubclass, Jt>, remap_iterator_base<Subclass, It>>::value>::type,
                class = typename std::enable_if<std::is_convertible<Uarget *, target_type *>::value>::type>
        inline target_remap_iterator_base(target_remap_iterator_base<Tubclass, Jt, Uarget> const &trib)
                : remap_iterator_base<Subclass, It>(trib.inner()), _target(&trib.target()) {}

        template<class ...Args>
        inline target_remap_iterator_base(target_type &target, Args &&... args)
                : remap_iterator_base<Subclass, It>(inner_iterator(std::forward<Args>(args)...)), _target(&target) {}

    private:
        target_type *_target;
    };

    template<class Container, class It>
    class map_item_iterator : public target_remap_iterator_base<map_item_iterator<Container, It>, It, Container> {
        using base_type = target_remap_iterator_base<map_item_iterator<Container, It>, It, Container>;
    public:
        using container_type = Container;
        using typename target_remap_iterator_base<map_item_iterator<Container, It>, It, Container>::inner_iterator;

        using iterator_category = typename std::iterator_traits<It>::iterator_category;
        using difference_type = typename std::iterator_traits<It>::difference_type;
        using reference = typename std::conditional<std::is_const<container_type>::value,
                typename container_type::const_reference, typename container_type::reference>::type;
        using const_reference = typename container_type::const_reference;
        using value_type = typename std::remove_reference<reference>::type;
        using pointer = value_type *;
        using base_type::target;
        using base_type::inner;
        using base_type::base_type;

        inline reference operator*() {
            return target()[*inner()];
        }

        inline const_reference operator*() const {
            return target()[*inner()];
        }

        template<class T = reference, class = typename std::enable_if<std::is_reference<T>::value>::type>
        inline pointer operator->() {
            return &(target()[*inner()]);
        }

        template<class T = const_reference, class = typename std::enable_if<std::is_reference<T>::value>::type>
        inline const pointer operator->() const {
            return &(target()[*inner()]);
        }
    };

    template<class It>
    class simple_range {
        It _b, _e;
    public:
        inline simple_range(It const &begin, It const &end) : _b(begin), _e(end) {}

        inline simple_range(It &&begin, It &&end) : _b(std::move(begin)), _e(std::move(end)) {}

        inline It begin() const { return _b; }

        inline It end() const { return _e; }
    };


    template<class T>
    class pseudo_fwd_iterator {
    public:
        virtual void advance() = 0;

        virtual bool is_past_end() const = 0;

        virtual bool equals(pseudo_fwd_iterator<T> const &other) const = 0;

        virtual T dereference() const = 0;

        virtual std::unique_ptr<pseudo_fwd_iterator<T>> copy() const = 0;

        virtual ~pseudo_fwd_iterator() = default;
    };

    template<class T>
    class pseudo_fwd_iterator_wrapper {
        std::unique_ptr<pseudo_fwd_iterator<T>> _pseudo;
    public:
        using iterator_category = std::forward_iterator_tag;
        using reference = T;
        using value_type = T;
        using difference_type = std::ptrdiff_t;
        using pointer = void;

        inline pseudo_fwd_iterator_wrapper() = default;

        inline pseudo_fwd_iterator_wrapper(pseudo_fwd_iterator_wrapper &&) noexcept = default;

        inline pseudo_fwd_iterator_wrapper(std::unique_ptr<pseudo_fwd_iterator<T>> &&pseudo) : _pseudo(
                std::move(pseudo)) {}

        inline pseudo_fwd_iterator_wrapper(pseudo_fwd_iterator_wrapper const &other) {
            if (other._pseudo != nullptr) {
                _pseudo = other._pseudo->copy();
            }
        }

        inline T operator*() const { return _pseudo->dereference(); }

        inline pseudo_fwd_iterator_wrapper &operator++() {
            _pseudo->advance();
            return *this;
        }

        inline pseudo_fwd_iterator_wrapper operator++(int) {
            pseudo_fwd_iterator_wrapper copy(*this);
            ++(*this);
            return copy;
        }

        inline bool operator==(pseudo_fwd_iterator_wrapper const &other) const {
            if (other._pseudo == nullptr) {
                return _pseudo == nullptr or is_past_end();
            } else if (_pseudo != nullptr) {
                return _pseudo->equals(*other._pseudo);
            }
            return false;
        }

        inline bool operator!=(pseudo_fwd_iterator_wrapper const &other) const {
            return not(*this == other);
        }

        inline pseudo_fwd_iterator_wrapper &operator=(pseudo_fwd_iterator_wrapper const &other) {
            if (other._pseudo != _pseudo) {
                _pseudo.reset();
                if (other._pseudo != nullptr) {
                    _pseudo = other._pseudo->copy();
                }
            }
            return *this;
        }

        inline pseudo_fwd_iterator_wrapper &operator=(pseudo_fwd_iterator_wrapper &&) noexcept = default;

        inline bool is_past_end() const { return _pseudo->is_past_end(); }
    };

}

#endif //HE_ITERATORS_H
