//
//   Copyright 2017 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 08/09/16.
//
/** @file adj_list.h
 * @brief "Fast" implementation of an adjacency list container.
 */

#ifndef HE_ADJ_LIST_H
#define HE_ADJ_LIST_H

#include <vector>
#include "graph_base.h"

namespace he {

    /** @brief Base class of an adjacency list container.
     *
     * This class behaves like a contiguous vector storing objects of type `T`. In this container, each element has an
     * associated direction @ref arc_dir, either `incoming`, `loop` or `outgoing`. The objects are stored contiguously,
     * first the ones with type `incoming`, followed by those with type `loop` and at last those with type `outgoing`.
     *
     * The position in the internal storage of the objects is variable, and subclasses must override the method
     * @ref update_reference. This method is called every time an element of the list is changed in position. This
     * happens upon insertion or deletion of an element in the list.
     *
     * The way this is implemented, is that each insertion or addition requires time O(1), because we swap two elements
     * and we push or pop from the back of the internal storage (a vector) in time O(1). Refer to @ref push and
     * @ref pop for more details.
     *
     * @tparam T The type of object stored in the list. Must be default constructible.
     */
    template<class T>
    class adj_list {
    public:
        /** @brief Value type held by the internal storage (a vector).
         */
        using value_type = typename std::vector<T>::value_type;

        /** @brief Reference type returned by the internal storage (a vector).
         */
        using reference = typename std::vector<T>::reference;

        /** @brief Constant reference type returned by the internal storage (a vector).
         */
        using const_reference = typename std::vector<T>::const_reference;

        virtual ~adj_list() = default;

    private:
        std::vector<T> _v;
        weak_idx_t _n_in;
        weak_idx_t _n_loops;

        /**
         * Performs the needed swaps to move @p from onto @p to, and calls to @ref update_reference forwarding the
         * parameter @p to_type.
         */
        void move_item(weak_idx_t from, weak_idx_t to, arc_dir to_type);

    protected:
        /** @brief Updates the stored index of @p item to @p new_ref.
         *
         * This is called when an element in the adjacency list is assigned a new index. When this happens, this
         * callback is called.
         *
         * @param type Type of the object that has been changed of index.
         * @param item Reference to the object that has been changed of index.
         * @param new_ref New index of @p reference.
         */
        virtual void update_reference(arc_dir type, reference item, weak_idx_t new_ref) = 0;

    public:
        /** @brief Initializes a new, empty adjacency list.
         */
        inline adj_list();

        /** @brief Returns a const reference to the element at index @p i.
         * Refer to `std::vector::operator[]`.
         */
        inline const_reference operator[](weak_idx_t i) const;

        /** @brief Returns a reference to the element at index @p i.
         * Refer to `std::vector::operator[]`.
         */
        inline reference operator[](weak_idx_t i);

        /** @brief Returns a const reference to the element at index @p i.
         * Refer to `std::vector::at`.
         */
        inline const_reference at(weak_idx_t i) const;

        /** @brief Returns a reference to the element at index @p i.
         * Refer to `std::vector::at`.
         */
        inline reference at(weak_idx_t i);

        /** @brief Returns the number of objects of the given type @p dir that are present in the adjacency list.
         * Constant runtime.
         * @param dir The type of arcs to select. This can be any flag combination.
         */
        inline weak_idx_t num_arcs(arc_dir dir) const;

        /** @brief Computes the direction of element at index @p item.
         * Constant runtime.
         * @param item A valid index corresponding to an object in this adjancy list.
         * @throw If @p item is greater than the size of this container, throws `std::invalid_argument`.
         * @return One of `incoming`, `loop` or `outgoing`.
         */
        inline arc_dir dir(weak_idx_t item) const;

        /** @brief Returns any item of type @p dir, if any, otherwise a default constructed `T`.
         * Constant runtime.
         * @param dir Type of the item to look for.
         * @return One item of type @p dir if @ref num_arcs is greater than 0 for direction @p dir, otherwise a default
         * constructed `T`.
         */
        inline T any_item(arc_dir dir) const;

        /** @brief Returns the index of the first element of type @p type in the list.
         * Constant runtime.
         */
        inline weak_idx_t begin(arc_dir type) const;

        /** @brief Returns one plus the index of the last element of type @p type in the list.
         * Constant runtime.
         */
        inline weak_idx_t end(arc_dir type) const;

        /** @brief Inserts a new element in the list.
         *
         * A new place will be created at the end of the internal storage. For all the types greater than @p type, i.e.
         * `incoming < loop < outgoing`, the first element is moved into the last position to leave room for @p item.
         * This implies at most two swaps and two calls to @ref update_reference. All the other elements stay in place.
         * This yields a constant runtime insertion (except for the cases when the internal storage vector is resized).
         *
         * @param type Type of the arc to insert. Must be one of `incoming`, `loop` or `outgoing.`
         * @param item A constant reference to the item to insert into the list.
         * @return The index of the newly inserted item.
         * @throws `std::invalid_argument` if @p type is not one of `incoming`, `loop`, `outgoing`.
         * If the number of items exceeds those representable by @ref weak_idx_t, `std::runtime_error`.
         *
         * @note This method will call @ref update_reference for the newly inserted element.
         */
        inline weak_idx_t push(arc_dir type, T const &item);

        /** @brief Removes an element from the list.
         *
         * The position used by @p item is then used to move the last item with the same type as @p item onto. This
         * frees a position in the list of the type immediately following the type of @p item (i.e. `incoming < loop <
         * outgoing`). The procedure is then repeated by bringing the last element of this list in the freed position,
         * and so on and so forth until the last element in the internal storage vector is then freed. At this point, it
         * gets popped from the back. This yields a constant runtime deletion, with at most two swaps and calls to
         * @ref update_reference.
         *
         * @param item A (valid) index pointing to an item in this list.
         * @throw `std::out_of_range` if @p item is greater or equal to the number of stored elements in the list.
         * @return The direction of @p item when it was in the list.
         *
         * @note This method will call @ref update_reference for the soon-to-be deleted element, with an invalid index.
         */
        inline arc_dir pop(weak_idx_t item);
    };

    template<class T>
    void adj_list<T>::move_item(weak_idx_t from, weak_idx_t to, arc_dir to_type) {
        if (from == to) {
            return;
        }
        std::swap(_v.at(from), _v.at(to));
        update_reference(to_type, _v[to], to);
    }

    template<class T>
    adj_list<T>::adj_list() : _n_in(0), _n_loops(0) {}

    template<class T>
    typename adj_list<T>::const_reference adj_list<T>::operator[](weak_idx_t i) const {
        return _v[i];
    }

    template<class T>
    typename adj_list<T>::reference adj_list<T>::operator[](weak_idx_t i) {
        return _v[i];
    }

    template<class T>
    typename adj_list<T>::const_reference adj_list<T>::at(weak_idx_t i) const {
        return _v.at(i);
    }

    template<class T>
    typename adj_list<T>::reference adj_list<T>::at(weak_idx_t i) {
        return _v.at(i);
    }

    template<class T>
    weak_idx_t adj_list<T>::num_arcs(arc_dir dir) const {
        switch (dir) {
            case arc_dir::incoming:
                return _n_in;
            case arc_dir::loop:
                return _n_loops;
            case arc_dir::outgoing:
                return weak_idx_t(_v.size()) - _n_in - _n_loops;
            case arc_dir::any_outgoing:
                return weak_idx_t(_v.size()) - _n_in;
            case arc_dir::any_incoming:
                return _n_in + _n_loops;
            case arc_dir::any_no_loop:
                return weak_idx_t(_v.size()) - _n_loops;
            case arc_dir::any:
            default:
                return weak_idx_t(_v.size());
        }
    }

    template<class T>
    arc_dir adj_list<T>::dir(weak_idx_t item) const {
        if (item < _n_in) {
            return arc_dir::incoming;
        } else if (item < _n_in + _n_loops) {
            return arc_dir::loop;
        } else if (item < _v.size()) {
            return arc_dir::outgoing;
        } else {
            throw std::invalid_argument("Queried direction for an element outside the adjacency list.");
        }
    }

    template<class T>
    T adj_list<T>::any_item(arc_dir dir) const {
        if (num_arcs(dir) == 0) {
            return T();
        }
        if (dir & arc_dir::incoming) {
            return _v.front();
        } else {
            return _v.back();
        }
    }

    template<class T>
    weak_idx_t adj_list<T>::begin(arc_dir type) const {
        if (type & arc_dir::incoming) {
            return 0;
        } else if (type & arc_dir::loop) {
            return _n_in;
        } else {
            return _n_in + _n_loops;
        }
    }

    template<class T>
    weak_idx_t adj_list<T>::end(arc_dir type) const {
        if (type & arc_dir::outgoing) {
            return weak_idx_t(_v.size());
        } else if (type & arc_dir::loop) {
            return _n_in + _n_loops;
        } else {
            return _n_in;
        }
    }

    template<class T>
    weak_idx_t adj_list<T>::push(arc_dir type, T const &item) {
        if (_v.size() >= std::numeric_limits<weak_idx_t>::max()) {
            throw std::out_of_range("Exceeded the maximum number of representable arcs.");
        }
        weak_idx_t retval;
        switch (type) {
            case arc_dir::outgoing:
                _v.push_back(item);
                retval = weak_idx_t(_v.size() - 1);
                break;
            case arc_dir::loop: {
                // Make room for one item
                _v.emplace_back();
                const weak_idx_t insertion_pt = _n_in + _n_loops;
                move_item(insertion_pt, weak_idx_t(_v.size() - 1), arc_dir::outgoing);
                // Insert there
                _v[insertion_pt] = item;
                // Increase count
                ++_n_loops;
                retval = insertion_pt;
                break;
            }
            case arc_dir::incoming: {
                // Make room for one item and swap two
                _v.emplace_back();
                const weak_idx_t replacement_pt = _n_in + _n_loops;
                move_item(replacement_pt, weak_idx_t(_v.size() - 1), arc_dir::outgoing);
                move_item(_n_in, replacement_pt, arc_dir::loop);
                // Insert there
                _v[_n_in] = item;
                // Increase count
                retval = _n_in++;
                break;
            }
            default:
                throw std::invalid_argument("An arc has a mixed direction.");
        }
        update_reference(type, _v[retval], retval);
        return retval;
    }

    template<class T>
    arc_dir adj_list<T>::pop(weak_idx_t item) {
        if (item < _n_in) {
            update_reference(arc_dir::incoming, _v[item], std::numeric_limits<weak_idx_t>::max());
            // Incoming edge. Move the last outgoing edge on top of it
            const weak_idx_t replacement_pt_1 = _n_in - 1;
            move_item(replacement_pt_1, item, arc_dir::incoming);
            // Move the last loop on top of the last outgoing edge
            const weak_idx_t replacement_pt_2 = replacement_pt_1 + _n_loops;
            move_item(replacement_pt_2, replacement_pt_1, arc_dir::loop);
            // Move the last outgoing arc on top of the last loop
            move_item(weak_idx_t(_v.size() - 1), replacement_pt_2, arc_dir::outgoing);
            _v.pop_back();
            --_n_in;
            return arc_dir::incoming;
        } else if (item < _n_in + _n_loops) {
            update_reference(arc_dir::loop, _v[item], std::numeric_limits<weak_idx_t>::max());
            // Loop. Move the last loop on top of it
            const weak_idx_t replacement_pt = _n_in + _n_loops - 1;
            move_item(replacement_pt, item, arc_dir::loop);
            // And the last outgoing edge on top of it too.
            move_item(weak_idx_t(_v.size() - 1), replacement_pt, arc_dir::outgoing);
            _v.pop_back();
            --_n_loops;
            return arc_dir::loop;
        } else if (item < _v.size()) {
            update_reference(arc_dir::outgoing, _v[item], std::numeric_limits<weak_idx_t>::max());
            // Outgoing edge. Replace with something else
            move_item(weak_idx_t(_v.size() - 1), item, arc_dir::outgoing);
            _v.pop_back();
            return arc_dir::outgoing;
        } else {
            throw std::out_of_range("Attempt at removing an arc beyond the storage of the adjacency list.");
        }
    }

}


#endif //HE_ADJ_LIST_H
