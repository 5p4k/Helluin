//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 08/09/16.
//
/** @file graph_adj_list.h
 * @brief Adjacency list implementation of a graph.
 */

#ifndef HE_GRAPH_ADJ_LIST_H
#define HE_GRAPH_ADJ_LIST_H

#include "graph_parts.h"
#include "storage.h"
#include "adj_list.h"

/** namespace he */
namespace he {

    /** @brief Adjacency list implementation of the arc/edge storage part of a @ref graph_base.
     *
     * This class will use two storages, one attached to the node pool and one attached to the arc pool, to store
     * per-arc and per-node data.
     *
     * @tparam Storage The storage type to use internally for archiving the node/arc data.
     * @see al_node
     * @see al_arc
     * @see adj_list
     */
    template<template<class> class Storage = storage>
    class adj_list_graph : public virtual graph_base {
        template<class U>
        using storage_type = typename std::enable_if<
                std::is_base_of<storage_base<U>, Storage<U>>::value, Storage<U>>::type;

        class al_arc;
        class al_node;

        std::unique_ptr<storage_type<al_node>> _nodes;
        std::unique_ptr<storage_type<al_arc>> _arcs;

        inline storage_type<al_node> &nstorage();
        inline storage_type<al_node> const &nstorage() const;
        inline storage_type<al_arc> &astorage();
        inline storage_type<al_arc> const &astorage() const;
    public:
        using graph_base::endpoint;

        /** @brief Default-constructs an empty adjacency list graph.
         */
        inline adj_list_graph();

        /** @brief Constructs an empty adjacency list graph using the provided index pools.
         *
         * @param nodes An empty index pool to be used for allocating nodes.
         * @param arcs An empty index pool to be used for allocating nodes.
         *
         * @throw If any of the pools is non-empty (size, not capacity). It is impossible to allocated nodes and arcs
         * without knowing in advance which is adjacent to which.
         */
        inline adj_list_graph(std::unique_ptr<idx_pool_base> &&nodes, std::unique_ptr<idx_pool_base> &&arcs);

        /** @brief Implementation of @ref graph_base::endpoints. Constant runtime.
         * @see graph_base::endpoints
         * @throw This functions asserts whether @p i is allocated.
         */
        inline std::pair<idx<node>, idx<node>> endpoints(idx<arc> i) const override;

        /** @brief Implementation of @ref graph_base::endpoint. Constant runtime.
         * @see graph_base::endpoint
         * @throw This functions asserts whether @p i is allocated.
         */
        inline idx<node> endpoint(idx<arc> i, arc_endpt endpt) const override;

        /** @brief Implementation of @ref graph_base::degree. Constant runtime.
         * @see graph_base::degree
         * @throw This functions asserts whether @p in is allocated.
         */
        inline weak_idx_t degree(idx<node> in, arc_dir dir) const override;

        /** @brief Implementation of @ref graph_base::any_adjacent_arc. Constant runtime.
         * @see graph_base::any_adjacent_arc
         * @throw This functions asserts whether @p n is allocated.
         */
        inline idx<arc> any_adjacent_arc(idx<node> n, arc_dir dir) const override;

    protected:

        /**
         * Removes @p i from the list of adjacent arcs on both endpoints.
         * @see graph_base::before_delete
         */
        inline void before_delete(idx<arc> i) override;

        /**
         * Asserts that @p i does not have any adjacent arc.
         * @see graph_base::before_delete
         */
        inline void before_delete(idx<node> i) override;

        /**
         * Just sets the reference to the adjacency list graph stored in the @ref al_node at index @p i.
         * @see graph_base::touch
         */
        inline void touch(idx<node> i, idx<node> cloned_from) override;

        /**
         * Attaches or detaches an arc, consistently, to each endpoint which needs an update. Stores in the arc the
         * index of the arc itself in the adjacency lists of its adjacent @ref al_node.
         * @see graph_base::touch
         */
        inline void touch(idx<arc> i, idx<arc> clone_from, idx<node> from, idx<node> to) override;

        /**
         * Returns an iterator wrapping around the adjacent arcs of node @p n.
         * @see graph_base::enum_adj_arcs
         */
        inline std::unique_ptr<pseudo_adj_arcs_iterator> enum_adj_arcs(idx<node> n, arc_dir filter) const override;
    };


    /** @brief Storage class for an adjacency list arc.
     *
     *  An adjacency lits arc stores a pair of values for each of its endpoints: the corresponding `idx<node>`, and the
     *  index of the arc itself in the @ref al_node adjacency list. In this way the arc can be removed in constant time
     *  by swapping the index with another arc and popping the last element.
     *
     *  @note This class is not meant to be accessed in any way by outside.
     *
     *  @see al_node
     */
    template<template<class> class Storage>
    class adj_list_graph<Storage>::al_arc {
        idx<node> _head;
        idx<node> _tail;
        weak_idx_t _self_in_head;
        weak_idx_t _self_in_tail;
    public:
        /** @brief Default initializes an arc in an invalid state.
         * To make the arc valid, @ref set_endpt and @ref set_self need to be called for both endpoints.
         */
        inline al_arc();

        /** @brief Returns the index of the endpoint at @p endpt.
         */
        inline idx<node> endpt(arc_endpt endpt) const;

        /** @brief Sets the index of the endpoint at @p endpt.
         */
        inline void set_endpt(arc_endpt endpt, idx<node> n);

        /** @brief Returns the index of this arc in the adjacency list of the @ref al_node at endpoint @p endpt.
         */
        inline weak_idx_t self(arc_endpt endpt) const;

        /** @brief Sets the index of this arc in the adjacency list of the @ref al_node at endpoint @p endpt.
         */
        inline void set_self(arc_endpt endpt, weak_idx_t self);
    };

    /** @brief The internal implementation of an adjacency list node.
     *
     * This class contains an @ref adj_list of indices of arcs and a reference to the arc storage, to be able to access
     * the arcs and update their @ref al_arc::self property.
     *
     *  @note This class is not meant to be accessed in any way by outside.
     *  @see al_arc
     *  @see adj_list
     */
    template<template<class> class Storage>
    class adj_list_graph<Storage>::al_node : public adj_list<idx<arc>> {
        storage_type<al_arc> *_arc_storage;
    protected:
        /** @brief Implementation of @ref adj_list::update_reference.
         * Calls @ref al_arc::set_self on the appropriate arc corresponding to @p item.
         *
         * @see adj_list::update_reference
         */
        inline void update_reference(arc_dir type, idx<arc> &item, weak_idx_t new_ref) override;

    public:
        class delta_pseudo_iterator;

        /** @brief Constructs a new node in an invalid state.
         * To make the node valid, call @ref set_arc_storage.
         */
        inline al_node();

        /** @brief Constructs a valid new node with degree zero.
         *
         * @param arc_storage The paired arc storage which contains the instances of @ref al_arc. The storage reference
         * must outlive this @ref al_node object.
         */
        inline al_node(storage_type<al_arc> &arc_storage);

        /** @brief Sets the internal arc storage containing the instances of @ref al_arc.
         * The storage reference must outlive this @ref al_node object.
         */
        inline void set_arc_storage(storage_type<al_arc> &arc_storage);

        /** @brief Wrapper around @ref adj_list::pop which performs an assertion on the arc.
         * The assertion makes sure that the arc at index @p item has actually index @p expected.
         * @see adj_list::pop
         */
        inline arc_dir pop_safe(weak_idx_t item, idx<arc> __attribute__((unused)) expected);

    };


    /** @brief Implementation of @ref pseudo_adj_arcs_iterator which wraps around an @ref al_node.
     * @see graph_base::enum_adj_arcs
     */
    template<template<class> class Storage>
    class adj_list_graph<Storage>::al_node::delta_pseudo_iterator : public pseudo_adj_arcs_iterator {
        al_node const &_node;
        arc_dir _filter;
        weak_idx_t _i;
    public:
        /** @brief Constructs the pseudo iterator around @p node with filter @p filter.
         */
        inline delta_pseudo_iterator(al_node const &node, arc_dir filter);

        /** @brief Implementation of @ref pseudo_adj_arcs_iterator::is_past_end.
         * @see pseudo_adj_arcs_iterator::is_past_end
         */
        inline bool is_past_end() const override;

        /** @brief Implementation of @ref pseudo_adj_arcs_iterator::dereference.
         * @see pseudo_adj_arcs_iterator::dereference
         */
        inline std::pair<idx<arc>, arc_dir> dereference() const override;

        /** @brief Implementation of @ref pseudo_adj_arcs_iterator::advance.
         * @see pseudo_adj_arcs_iterator::advance
         */
        inline void advance() override;

        /** @brief Implementation of @ref pseudo_adj_arcs_iterator::equals.
         * @see pseudo_adj_arcs_iterator::equals
         */
        inline bool equals(pseudo_adj_arcs_iterator const &other) const override;

        /** @brief Implementation of @ref pseudo_adj_arcs_iterator::copy.
         * @see pseudo_adj_arcs_iterator::copy
         */
        inline std::unique_ptr<pseudo_adj_arcs_iterator> copy() const override;
    };


    template<template<class> class Storage>
    adj_list_graph<Storage>::al_node::delta_pseudo_iterator::delta_pseudo_iterator(al_node const &node, arc_dir filter)
            :
            _node(node),
            _filter(filter),
            _i(_node.begin(filter)) {
        if (filter == arc_dir::any_no_loop and _node.num_arcs(arc_dir::incoming) == 0) {
            _i = _node.begin(arc_dir::outgoing);
        }
    }

    template<template<class> class Storage>
    bool adj_list_graph<Storage>::al_node::delta_pseudo_iterator::is_past_end() const {
        return _i >= _node.end(_filter);
    }

    template<template<class> class Storage>
    std::pair<idx<arc>, arc_dir> adj_list_graph<Storage>::al_node::delta_pseudo_iterator::dereference() const {
        if (is_past_end()) {
            return {invalid_idx, arc_dir::any};
        } else {
            return {_node[_i], _node.dir(_i)};
        }
    }

    template<template<class> class Storage>
    void adj_list_graph<Storage>::al_node::delta_pseudo_iterator::advance() {
        if (not is_past_end()) {
            if (_filter == arc_dir::any_no_loop and _node.dir(_i) == arc_dir::loop) {
                _i = _node.end(arc_dir::loop);
            } else {
                ++_i;
            }
        }
    }

    template<template<class> class Storage>
    bool adj_list_graph<Storage>::al_node::delta_pseudo_iterator::equals(pseudo_adj_arcs_iterator const &other) const {
        delta_pseudo_iterator const *other_cast = dynamic_cast<delta_pseudo_iterator const *>(&other);
        return other_cast != nullptr and _i == other_cast->_i;
    }

    template<template<class> class Storage>
    std::unique_ptr<pseudo_adj_arcs_iterator> adj_list_graph<Storage>::al_node::delta_pseudo_iterator::copy() const {
        return std::unique_ptr<pseudo_adj_arcs_iterator>(new delta_pseudo_iterator(*this));
    }

    template<template<class> class Storage>
    void adj_list_graph<Storage>::al_node::update_reference(arc_dir type, idx<arc> &item, weak_idx_t new_ref) {
        assert(_arc_storage != nullptr);
        switch (type) {
            case arc_dir::incoming:
                (*_arc_storage)[item].set_self(arc_endpt::head, new_ref);
                break;
            case arc_dir::outgoing:
                (*_arc_storage)[item].set_self(arc_endpt::tail, new_ref);
                break;
            case arc_dir::loop:
                (*_arc_storage)[item].set_self(arc_endpt::head, new_ref);
                (*_arc_storage)[item].set_self(arc_endpt::tail, new_ref);
                break;
            default:
                throw std::invalid_argument("An arc has a mixed direction.");
        }
    }

    template<template<class> class Storage>
    adj_list_graph<Storage>::al_node::al_node() : _arc_storage(nullptr) {}

    template<template<class> class Storage>
    adj_list_graph<Storage>::al_node::al_node(storage_type<al_arc> &arc_storage) : _arc_storage(&arc_storage) {}

    template<template<class> class Storage>
    void adj_list_graph<Storage>::al_node::set_arc_storage(storage_type<al_arc> &arc_storage) {
        assert(_arc_storage == nullptr or _arc_storage == &arc_storage);
        _arc_storage = &arc_storage;
    }

    template<template<class> class Storage>
    arc_dir adj_list_graph<Storage>::al_node::pop_safe(weak_idx_t item, idx<arc> __attribute__((unused)) expected) {
        assert(at(item) == expected);
        return pop(item);
    }


    template<template<class> class Storage>
    adj_list_graph<Storage>::al_arc::al_arc() : _head(invalid_idx), _tail(invalid_idx),
                                                _self_in_head(std::numeric_limits<weak_idx_t>::max()),
                                                _self_in_tail(std::numeric_limits<weak_idx_t>::max()) {}

    template<template<class> class Storage>
    idx<node> adj_list_graph<Storage>::al_arc::endpt(arc_endpt endpt) const {
        return endpt == arc_endpt::head ? _head : _tail;
    }

    template<template<class> class Storage>
    void adj_list_graph<Storage>::al_arc::set_endpt(arc_endpt endpt, idx<node> n) {
        if (endpt == arc_endpt::head) {
            assert(not _head or not n);
            _head = n;
        } else {
            assert(not _tail or not n);
            _tail = n;
        }
    }

    template<template<class> class Storage>
    weak_idx_t adj_list_graph<Storage>::al_arc::self(arc_endpt endpt) const {
        if (endpt == arc_endpt::head) {
            assert(_head);
            return _self_in_head;
        } else {
            assert(_tail);
            return _self_in_tail;
        }
    }

    template<template<class> class Storage>
    void adj_list_graph<Storage>::al_arc::set_self(arc_endpt endpt, weak_idx_t self) {
        if (endpt == arc_endpt::head) {
            assert(_head);
            _self_in_head = self;
        } else {
            assert(_tail);
            _self_in_tail = self;
        }
    }



    template<template<class> class Storage>
    adj_list_graph<Storage>::adj_list_graph() : graph_base(),
                                                _nodes(new storage_type<al_node>()),
                                                _arcs(new storage_type<al_arc>())
    {
        nstorage().attach_to(node_pool());
        astorage().attach_to(arc_pool());
    }

    template<template<class> class Storage>
    adj_list_graph<Storage>::storage_type<typename adj_list_graph<Storage>::al_node> &
    adj_list_graph<Storage>::nstorage() {
        return *_nodes;
    }

    template<template<class> class Storage>
    adj_list_graph<Storage>::storage_type<typename adj_list_graph<Storage>::al_node> const &
    adj_list_graph<Storage>::nstorage() const {
        return *_nodes;
    }

    template<template<class> class Storage>
    adj_list_graph<Storage>::storage_type<typename adj_list_graph<Storage>::al_arc> &
    adj_list_graph<Storage>::astorage() {
        return *_arcs;
    }

    template<template<class> class Storage>
    adj_list_graph<Storage>::storage_type<typename adj_list_graph<Storage>::al_arc> const &
    adj_list_graph<Storage>::astorage() const {
        return *_arcs;
    }

    template<template<class> class Storage>
    adj_list_graph<Storage>::adj_list_graph(std::unique_ptr<idx_pool_base> &&nodes,
                                            std::unique_ptr<idx_pool_base> &&arcs) :
            graph_base(std::move(nodes), std::move(arcs)) {
        if (node_pool().size() > 0) {
            throw std::runtime_error("Cannot initialize an adjacency list graph with a nonempty pool of nodes.");
        }
        if (arc_pool().size() > 0) {
            throw std::runtime_error("Cannot initialize an adjacency list graph with a nonempty pool of arcs.");
        }
        nstorage().attach_to(node_pool());
        astorage().attach_to(arc_pool());
    }

    template<template<class> class Storage>
    std::pair<idx<node>, idx<node>> adj_list_graph<Storage>::endpoints(idx<arc> i) const {
        assert(arc_pool().is_allocated(i));
        al_arc const &a = astorage()[i];
        return {a.endpt(arc_endpt::tail), a.endpt(arc_endpt::head)};
    }

    template<template<class> class Storage>
    idx<node> adj_list_graph<Storage>::endpoint(idx<arc> i, arc_endpt endpt) const {
        assert(arc_pool().is_allocated(i));
        return astorage()[i].endpt(endpt);
    }

    template<template<class> class Storage>
    weak_idx_t adj_list_graph<Storage>::degree(idx<node> in, arc_dir dir) const {
        al_node const &n = nstorage()[in];
        return n.num_arcs(dir);
    }

    template<template<class> class Storage>
    idx<arc> adj_list_graph<Storage>::any_adjacent_arc(idx<node> n, arc_dir dir) const {
        if (not node_pool().is_allocated(n)) {
            return invalid_idx;
        } else {
            return nstorage()[n].any_item(dir);
        }
    }


    template<template<class> class Storage>
    void adj_list_graph<Storage>::before_delete(idx<arc> i) {
        al_arc &a = astorage()[i];
        // Disconnect, only once if it's a loop
        if (nstorage().at(a.endpt(arc_endpt::tail)).pop_safe(a.self(arc_endpt::tail), i) != arc_dir::loop) {
            nstorage().at(a.endpt(arc_endpt::head)).pop_safe(a.self(arc_endpt::head), i);
        }
    }

    template<template<class> class Storage>
    void adj_list_graph<Storage>::before_delete(idx<node> i) {
        assert(nstorage()[i].num_arcs(arc_dir::any) == 0);
    }

    template<template<class> class Storage>
    void adj_list_graph<Storage>::touch(idx<node> i, idx<node> cloned_from) {
        // Initialize
        nstorage()[i].set_arc_storage(astorage());
    }

    template<template<class> class Storage>
    void adj_list_graph<Storage>::touch(idx<arc> i, idx<arc> clone_from, idx<node> from, idx<node> to) {
        al_arc &dest = astorage()[i];

        // Change of type will require complete disconnection
        const bool was_loop = dest.endpt(arc_endpt::tail) == dest.endpt(arc_endpt::head);

        // Change only if there a new from or to, or a clone, or a difference
        bool connect_tail = (from and dest.endpt(arc_endpt::tail) != from) or clone_from;
        bool connect_head = (to and dest.endpt(arc_endpt::head) != to) or clone_from;

        // Disconnect if this is not a clone, there was already an endpt, and either this endpt changes, or the
        // other does, but this was a loop.
        bool disconnect_tail =
                not clone_from and dest.endpt(arc_endpt::tail) and (connect_tail or (connect_head and was_loop));
        bool disconnect_head =
                not clone_from and dest.endpt(arc_endpt::head) and (connect_head or (connect_tail and was_loop));

        assert(clone_from or not(connect_tail and not from));
        assert(clone_from or not(connect_head and not to));

        // Fill in the missing values
        if (not from) {
            from = (clone_from ? astorage()[clone_from] : dest).endpt(arc_endpt::tail);
            assert(from);
        }
        if (not to) {
            to = (clone_from ? astorage()[clone_from] : dest).endpt(arc_endpt::head);
            assert(to);
        }

        assert(not was_loop or (disconnect_head == disconnect_tail));
        assert(not clone_from or (not disconnect_head and not disconnect_tail and connect_head and connect_tail));

        // Do the disconnection part. Maybe the was_loop & sth condition toggled disconnect_* on, while connect_*
        // is still off. Therefore we update the flags manually.
        if (disconnect_tail) {
            if (nstorage()[dest.endpt(arc_endpt::tail)].pop_safe(dest.self(arc_endpt::tail), i) == arc_dir::loop) {
                // Whoops we already popped it. Don't pop the head and signal that it must be reconnected.
                dest.set_endpt(arc_endpt::head, invalid_idx);
                disconnect_head = false;
                connect_head = true;
            }
            dest.set_endpt(arc_endpt::tail, invalid_idx);
            connect_tail = true;
        }
        if (disconnect_head) {
            __attribute__((unused)) arc_dir result = nstorage()[dest.endpt(arc_endpt::head)].pop_safe(
                    dest.self(arc_endpt::head), i);
            assert(result != arc_dir::loop);
            dest.set_endpt(arc_endpt::tail, invalid_idx);
            connect_head = true;
        }

        // Do the connection part
        if (from == to) {
            if (connect_head or connect_tail) {
                dest.set_endpt(arc_endpt::tail, from);
                dest.set_endpt(arc_endpt::head, to);
                nstorage()[from].push(arc_dir::loop, i);
            }
        } else {
            if (connect_tail) {
                dest.set_endpt(arc_endpt::tail, from);
                nstorage()[from].push(arc_dir::outgoing, i);
            }
            if (connect_head) {
                dest.set_endpt(arc_endpt::head, to);
                nstorage()[to].push(arc_dir::incoming, i);
            }
        }
    }

    template<template<class> class Storage>
    std::unique_ptr<pseudo_adj_arcs_iterator> adj_list_graph<Storage>::enum_adj_arcs(idx<node> n, arc_dir filter) const
    {
        return std::unique_ptr<pseudo_adj_arcs_iterator>(
                new typename al_node::delta_pseudo_iterator(nstorage()[n], filter));
    }
}

#endif //HE_GRAPH_ADJ_LIST_H
