//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 12/11/2016.
//

#ifndef HELLUIN_ALGO_H
#define HELLUIN_ALGO_H

#include <iterator>
#include <graph_base.h>
#include <storage.h>

namespace he {

    template<typename Fn>
    void quick_dfs(graph_base const &g, idx <node> root, Fn &&action, arc_dir dir = arc_dir::any) {
        storage<bool> visited(false);
        visited.attach_to(g.node_pool());

        std::function<void(idx<node>)> dfs_internal = [&](idx<node> current)
        {
            action(g, current);
            visited[current] = true;
            for (auto adj_arc_node_pair : g.adjacent_arcs(current, dir)) {
                idx<node> adj_node = g.endpoint(adj_arc_node_pair);
                if (not visited[adj_node]) {
                    dfs_internal(adj_node);
                }
            }
        };

        dfs_internal(root);
    }

}

#endif //HELLUIN_ALGO_H
