//
//   Copyright 2017 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 18/04/17.
//
/** @file graph_traits.h
 * @brief Some type traits for extracting information from a graph type.
 */

#ifndef HE_GRAPH_TRAITS_H
#define HE_GRAPH_TRAITS_H

#include "graph_base.h"
#include "graph_parts.h"

/** namespace he */
namespace he {
    /** @cond */
    template<class>
    struct idx;

    template<class, class>
    class store_nodes;

    template<class, class>
    class store_arcs;
    /** @endcond */

    /** @brief Struct to extract node/arc/storage types and references from a graph type. This is a type trait.
     *
     * This trait is undefined if the template parameter does not pass any of the following checks:
     *  - is a subclass of @ref graph_base
     *  - `G::node_type` is void *or* `G` inherits from @ref store_nodes, specifically
     *    `store_nodes<G::node_type, G::node_storage_type`.
     *  - `G::arc_type` is void *or* `G` inherits from @ref store_arcs, specifically
     *    `store_arcs<G::arc_type, G::arc_storage_type`.
     *
     * @tparam G An instance of a graph.
     */
    template<class G>
    struct graph_traits {
        /** @brief Type of node payload if present, or void.
         */
        using node_type = typename G::node_type;

        /** @brief Type of node payload storage if present, or void.
         * Should be some subclass of @ref storage_base with type @ref node_type.
         */
        using node_storage_type = typename G::node_storage_type;

        /** @brief Reference to a node payload if present, or void.
         */
        using node_reference = typename G::node_reference;

        /** @brief Pointer to a node payload if present, or void.
         */
        using node_pointer = typename G::node_pointer;

        /** @brief Const reference to a node payload if present, or void.
         */
        using node_const_reference = typename G::node_const_reference;

        /** @brief Const pointer to a node payload if present, or void.
         */
        using node_const_pointer = typename G::node_const_pointer;

        /** @brief Type of arc payload if present, or void.
         */
        using arc_type = typename G::arc_type;

        /** @brief Type of arc payload storage if present, or void.
         * Should be some subclass of @ref storage_base with type @ref node_type.
         */
        using arc_storage_type = typename G::arc_storage_type;

        /** @brief Reference to a arc payload if present, or void.
         */
        using arc_reference = typename G::arc_reference;

        /** @brief Pointer to a arc payload if present, or void.
         */
        using arc_pointer = typename G::arc_pointer;

        /** @brief Const reference to a arc payload if present, or void.
         */
        using arc_const_reference = typename G::arc_const_reference;

        /** @brief Const pointer to a arc payload if present, or void.
         */
        using arc_const_pointer = typename G::arc_const_pointer;

        /** @brief The very same graph type, if passes the checks.
         * @see graph_traits
         */
        using graph_type = typename std::enable_if<
                std::is_base_of<graph_base, G>::value and
                (std::is_void<node_type>::value or
                 std::is_base_of<store_nodes<node_type, node_storage_type>, G>::value) and
                (std::is_void<arc_type>::value or std::is_base_of<store_arcs<arc_type, arc_storage_type>, G>::value),
                G>::type;
    };

}

#endif //HE_GRAPH_TRAITS_H
