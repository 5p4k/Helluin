//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 06/08/16.
//
/** @file storage_base.h
 * @brief Basic definitions of index pools and attachable storage.
 */

#ifndef HE_STORAGE_BASE_H
#define HE_STORAGE_BASE_H

#include <vector>
#include <list>
#include <stdexcept>
#include <cassert>
#include "idx.h"
#include "type_traits.h"

/** @namespace he */
namespace he {

    class chi_iterator;
    class idx_pool_base;

    /** @brief Abstract base class defining a vector that automatically adapts to a given index pool.
     *
     * The purpose of an attachable object is to make sure that all the allocated items in an index pool have an
     * associated element always available in all attachables, identifiable by that very same index.
     *
     * An attachable object can be attached to only one pool at a time. This base class defined the attach/detach
     * semantic; subclasses are required to implement
     *
     *  - @ref allocate `void allocate(std::size_t idx)`
     *  - @ref clone `void clone(std::size_t idx, std::size_t orig)`
     *  - @ref destroy `void destroy(std::size_t idx)`
     *  - @ref relocate `void relocate(std::size_t old_idx, std::size_t new_idx)`
     *
     * And moreover they can implement the following functions:
     *
     *  - @ref after_attach `void after_attach()`
     *  - @ref before_detach `void before_detach()`
     *
     * to automatically synchronize their contents to the index pool.
     */
    class attachable_base {
        friend class idx_pool_base;

        idx_pool_base const *_pool;
        std::list<attachable_base *>::iterator _self;
    public:
        /** @brief Forbid copying.
         *
         * This would imply automatically attaching and copying the content of the whole object.
         */
        attachable_base(attachable_base const &) = delete;

        /** @brief Initializes a new attachable, attached to no class.
         */
        inline attachable_base();

        /** @brief Returns a pointer to the pool to which this object is attached, if there is one.
         * @return A pointer to the pool to which this object is attached or `nullptr` if there is none.
         */
        inline idx_pool_base const *pool() const;

        /** @brief Attached this object to @p pool.
         *
         * This is just an alias to `pool.attach(*this)`.
         * @see idx_pool_base::attach
         */
        inline void attach_to(idx_pool_base const &pool);

        /** @brief Detached this object from @ref pool.
         *
         * This is just an alias to `pool().detach(*this)`. If this object is not attached to anything, no operation is
         * performed.
         * @see idx_pool_base::detach
         */
        inline void detach();

        /** @brief True if and only if this class is attached to anything.
         *
         * This is equivalent to `pool() != nullptr`.
         * @return A boolean representing whether this class is attached to an index pool or not.
         * @see pool
         */
        inline bool is_attached() const;

        /** @brief Detaches the object before destruction.
         */
        inline virtual ~attachable_base();

    protected:
        /** @brief Called after this object has been attached to an index pool.
         *
         * At this point in time, @ref pool will return a nonnull value. Subclasses should use this method to resize
         * their containers to match the @ref idx_pool_base::capacity of the new @ref pool. The current implementation
         * registers this object at the owning index pool, therefore subclasses must call this
         * @ref attachable_base::after_attach method in their implementation.
         *
         * This function is called by @ref idx_pool_base::attach.
         * @see idx_pool_base::attach.
         */
        virtual inline void after_attach();

        /** @brief Called before detaching this object.
         *
         * At this point in time, @ref pool will return a nonnull value. Subclasses should use this method to clear
         * their internal containers. The current implementation deregisters this object at the owning index pool,
         * therefore subclasses must call this @ref attachable_base::before_detach method in their implementation.
         *
         * This function is called by @ref idx_pool_base::detach.
         * @see idx_pool_base::detach.
         */
        virtual inline void before_detach();

        /** @brief Inserts a new entry in the attachable at index @p idx.
         *
         * No information is given on how this entry should be constructed, this is up to the subclasses, e.g. taking
         * a default argument, initialize a pointer to `nullptr`, etc.; it is guaranteed that
         * `@p idx < pool()->capacity()` and that `pool().is_allocated(@p idx)` is true.
         *
         * This function is called by @ref idx_pool_base::allocate.
         *
         * @param idx Index at which to insert a new item.
         * @see idx_pool_base::allocate.
         */
        virtual void allocate(std::size_t idx) = 0;

        /** @brief Inserts a new entry in the attachable at index @p idx, by cloning data from @p orig.
         *
         * It is guaranteed that `@p idx < pool()->capacity()` and that `pool()->is_allocated(@p idx)` is true. it is
         * guaranteed as well that `pool()->is_allocated(@p orig)` is true (which implies
         * `@p orig < pool()->capacity()`).
         *
         * This function is called by @ref idx_pool_base::allocate.
         *
         * @param idx Index at which to insert a cloned item.
         * @param orig Item to clone on @p idx.
         * @see idx_pool_base::clone
         */
        virtual void clone(std::size_t idx, std::size_t orig) = 0;

        /** @brief Destroys an element at index @p idx.
         *
         * The implementation may choose simply to do nothing, and reset the data only if the index is reallocated.
         * After this call, @p idx is considered deallocate.
         *
         * This function is called by @ref idx_pool_base::destroy.
         *
         * @param idx Index of the object to destroy.
         * @see idx_pool_base::destroy
         */
        virtual void destroy(std::size_t idx) = 0;

        /** @brief Moves the element at position @p old_idx to @p new_idx.
         *
         * This function is used when changing the index of an allocated element, e.g. for compressing the memory.
         *
         * @todo Implement a `compress` routine in @ref idx_pool_base.
         * @param old_idx Index of an allocated element that needs to be moved.
         * @param new_idx An unallocated index below @ref idx_pool_base::size on which the content needs to be moved.
         */
        inline virtual void relocate(std::size_t __attribute__((unused)) old_idx,
                                     std::size_t __attribute__((unused)) new_idx)
        {}

        /** @brief Discards the memory beyond size().
         *
         * Every memory reserved for items with index greater than @p last_idx can be discarded, e.g. after compression.
         *
         * @todo This is currently unused, implement a `compress` routine in @ref idx_pool_base.
         *
         * @param last_idx The last allocated index.
         */
        inline virtual void truncate(std::size_t __attribute__((unused)) last_idx) { }
    };

    /** @brief Abstract base class defining an index pool.
     *
     * An index pool is a class capable of allocating and deallocating indices. Indices are used to persistently
     * identify objects, therefore the set of currently allocated indices may not be contiguous. An index pool can have
     * several @ref attachable_base objects attached to it, which are dynamically notified whenever an index
     * is allocated or deallocated.
     *
     * An index pool is able to reuse deallocated indices, thus has a @ref capacity, which is the maximum number of
     * available indices so far, and a @ref size, which is the number of indices currently allocated.
     *
     * An index pool makes a distinction from @ref allocate and @ref clone; when @ref clone is called, all the
     * attachables are informed that they need to copy the data.
     *
     * The index pool is the backbone of a @ref graph_base, because allows permanent node and arc indexing and per-arc
     * and per-node storage via subclasses of attachable.
     *
     * Subclasses are required to implement at least
     *
     *  - @ref is_allocated `bool is_allocated(std::size_t i) const`
     *
     * And they should as well provide a non-trivial implementation of
     *
     *  - @ref get_allocation_idx `std::size_t get_allocation_idx()`
     *
     * They can override as well
     *
     *  - @ref get_first_allocated_entry `std::size_t get_first_allocated_entry(std::size_t begin) const`
     *  - @ref before_allocate `void before_allocate(std::size_t i)`
     *  - @ref after_destroy `void after_destroy(std::size_t i)`
     *
     * For either performance reasons or bookkeeping.
     *
     * @see idx_pool
     * @see storage
     */
    class idx_pool_base {
    public:
        /** @brief Delete copy constructor.
         *
         * It is not possible to have an @ref attachable_base attached to two pools at the same time, therefore it is not
         * possible to fully copy an index pool. For this reason, the copy constructor has been disabled.
         */
        idx_pool_base(idx_pool_base const &) = delete;

        /** @brief Construct an empty index pool with no attachable.
         */
        inline idx_pool_base() : _cap(0), _sz(0) {}

        /** @brief The total number of indices currently available, allocated and not.
         *
         * It always olds `size() <= capacity()`.
         * @see size
         */
        inline std::size_t capacity() const { return _cap; }

        /** @brief The total amount of indices currently allocated.
         *
         * It always olds `size() <= capacity()`.
         * @see capacity
         */
        inline std::size_t size() const { return _sz; }

        /** @brief Attaches @p attachable to this pool.
         *
         * After @p attachable has been attached, the only action that is performed is
         * invoking @ref attachable_base::after_attach. Attaching an object that is already attached to this pool will
         * do nothing. An object that is already attached to another pool will be detached first.
         * @see attachable_base::after_attach
         */
        void attach(attachable_base &attachable) const;

        /** @brief Detaches @p attachable from this pool.
         *
         * Before completing the operation, @ref attachable_base::before_detach is called. No other operation is
         * performed on @p attachable.
         * @see attachable_base::before_detach
         */
        void detach(attachable_base &attachable) const;

        /** @brief Allocates a new index.
         *
         * This method will ask @ref get_allocation_idx which index to allocate, then call @ref before_allocate on it,
         * and utimately call @ref attachable_base::allocate on all the attached storages. The @ref capacity is
         * automatically enlarged to fit the new allocated index.
         *
         * @returns The newly allocated index.
         */
        std::size_t allocate();

        /** @brief Allocates a new index by notifying all the attachables that data must be cloned from @p other.
         *
         * This method is identical to @ref allocate, but @ref attachable_base::clone will be called instead of
         * @ref attachable_base::allocate.
         *
         * @param other Item to be cloned from.
         * @returns the newly allocated index.
         * @see allocate
         */
        std::size_t clone(std::size_t other);

        /** @brief Deallocated @p i on all the attachables.
         *
         * This method will cause @ref size to decrease by one; @ref capacity will remain constant.
         *
         * @param i Item to destroy.
         * @see capacity
         * @see size
         * @see allocate
         */
        void destroy(std::size_t i);

        /** @brief Returns `true` if and only if @p i is an allocated index.
         *
         * Subclasses must keep track of the allocation table for the indices and implement this function. This function
         * is called very frequently in the code and it should be O(1). For @p i greater or equal than @p capacity,
         * return `false`. To keep track of the allocation table, subclasses should override @ref before_allocate and
         * @ref after_destroy.
         *
         * @param i Item to be checked for allocation.
         * @return `true` if and only if @p i is allocated.
         */
        virtual bool is_allocated(std::size_t i) const = 0;

        /** @brief Returns an iterator pointing at the first allocated element.
         *
         * @see chi_iterator
         */
        inline chi_iterator begin() const;

        /** @brief Returns an iterator past the last allocated element.
         *
         * @see chi_iterator
         */
        inline chi_iterator end() const;

        /** @brief Returns the first allocated index greater or equal than @p begin.
         *
         * This function is used in @ref chi_iterator for obtaining the next allocated element during iteration. The
         * default implementation scans index by index starting at @p begin and invoking @ref is_allocated. Subclasses
         * can provide a different or faster implementation.
         *
         * @param begin First index to be checked for allocation.
         * @return A valid index `< @ref capacity` and `>= @ref begin` for which @ref is_allocated returns `true, if it
         * exists, otherwise `std::numeric_limits<std::size_t>::max()`.
         */
        inline virtual std::size_t get_first_allocated_entry(std::size_t begin) const;

        /** @brief Detaches all attachables before distruction.
         */
        inline virtual ~idx_pool_base();

    protected:
        /** @brief Called before @p i is allocated, both by @ref allocate and @ref clone.
         *
         * The default implementation enlarges @ref capacity to fit @p i and increments @ref size. Subclasses that
         * implement this function to update internal variables (e.g. a vector of booleans that holds
         * the @ref is_allocated value), must always call @ref idx_pool_base::before_allocate before exiting, to
         * ensure that the @ref capacity is correctly set.
         *
         * @param i Index that is going to be allocated; depends on the return value of @ref get_allocation_idx.
         */
        inline virtual void before_allocate(std::size_t i);

        /** @brief Called after @p i is destroyed, via @ref destroy.
         *
         * The default implementation reduces @ref size by one. Subclasses that implement this function must always
         * call @ref idx_pool_base::after_destroy to ensure that @ref size is correctly set.
         *
         * @param i Index that is going to be destroyed.
         */
        inline virtual void after_destroy(std::size_t i);

        /** @brief Returns a non-allocated index suitable to be used for a new allocation.
         *
         * The default implementation scans all the indices looking for one that is not allocated, and if none is found,
         * it will return @ref capacity. Subclasses should implement a faster version of this function.
         *
         * @return An index for which @ref is_allocated would return `false`, to be allocated.
         */
        virtual std::size_t get_allocation_idx() const;

    private:
        friend attachable_base::~attachable_base();
        void detach_internal(attachable_base &attachable) const;

        std::size_t _cap;
        std::size_t _sz;
        mutable std::list<attachable_base *> _attached;
    };

    /** @brief Augments an attachable with subscript operators.
     *
     * Any object that stores publicly accessible per-index data should inherit from @ref storage_base.
     * In principle an attachable object could do any bookkeeping operation. A special case is an attachable that
     * behaves like a vector; this is the reason why this class is defined.
     *
     * @tparam T Type contained.
     * @tparam Ref Reference type to `T`.
     * @tparam CRef Constant reference type to `T`.
     */
    template<class T, class Ref = T &, class CRef = T const &>
    class storage_base : public virtual attachable_base {
    public:
        using value_type = typename std::remove_reference<T>::type; ///< @brief Alias for `T`.
        using reference = Ref;                                      ///< @brief Alias fot `Ref`.
        using const_reference = CRef;                               ///< @brief Alias for `CRef`.

        /** @brief Access element at index @p i.
         *
         * This function should assume that @p i is allocated in the attached pool. It is undefined behavior to call
         * this with a unallocated index. For checking whether @p i is allocated, see @ref at.
         *
         * @param i Index to be accessed.
         * @return A constant reference to the value type at index @p i.
         * @see at
         */
        virtual const_reference operator[](std::size_t i) const = 0;

        /** @brief Access element at index @p i.
         *
         * This function should assume that @p i is allocated in the attached pool. It is undefined behavior to call
         * this with a unallocated index. For checking whether @p i is allocated, see @ref at.
         *
         * @param i Index to be accessed.
         * @return A reference to the value type at index @p i.
         * @see at
         */
        virtual reference operator[](std::size_t i) = 0;

        /** @brief Analogous of @ref operator[], with an added allocation test.
         *
         * This function just calls @ref operator[], but before checks if @p i is allocated.
         * @throws std::out_of_range if @p i is not  allocated.
         *
         * @param i Index to be accessed.
         * @return A constant reference to the value type at index @p i.
         * @see operator[]
         */
        inline const_reference at(std::size_t i) const;

        /** @brief Analogous of @ref operator[], with an added allocation test.
         *
         * This function just calls @ref operator[], but before checks if @p i is allocated.
         * @throws std::out_of_range if @p i is not  allocated.
         *
         * @param i Index to be accessed.
         * @return A reference to the value type at index @p i.
         * @see operator[]
         */
        inline reference at(std::size_t i);
    };

    /** @brief A forward iterator that loops over all the allocated elements in an @ref idx_pool_base.
     *
     * This class repeatedly calls @ref idx_pool_base::get_first_allocated_entry to obtain the next allocated element
     * until it returns the maximum value for `std::size_t`.
     *
     * The value type of this iterator is the index itself, it does not return a boolean representing whether the
     * index is allocated or not.
     *
     * Two past-the-end iterators evaluate equal even though their pools may be different.
     */
    class chi_iterator {
        idx_pool_base const *_pool;
        std::size_t _i;
    public:
        /** @cond */
        using value_type = std::size_t;
        using reference = std::size_t;
        using pointer = void;
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::forward_iterator_tag;
        /** @endcond */


        /** @brief Initializes a new, past-the-end iterator bound to no pool.
         *
         * Iterators constructed with this constructor always return `true` from @ref is_past_end.
         */
        inline chi_iterator() : _pool(nullptr), _i(std::numeric_limits<std::size_t>::max()) {}

        /** @brief Initializes a new iterator from @p pool.
         *
         * @param pool Pool to iterate over.
         * @param past_end True if the iterator should point to past-the-end in @p pool.
         */
        inline chi_iterator(idx_pool_base const &pool, bool past_end) : _pool(&pool),
                                                                        _i(std::numeric_limits<std::size_t>::max()) {
            if (not past_end) {
                _i = _pool->get_first_allocated_entry(0);
            }
        }

        /** @brief Returns true if and only if the iterator is beyond the last allocated element.
         */
        inline bool is_past_end() const {
            return _pool == nullptr or _i == std::numeric_limits<std::size_t>::max() or _i >= _pool->capacity();
        }

        /** @brief Returns the current index. It is undefined behavior to call this on an iterator that is past-the-end.
         */
        inline std::size_t operator*() const { return _i; }

        /** @cond */
        inline chi_iterator &operator++() {
            if (not is_past_end()) {
                _i = _pool->get_first_allocated_entry(_i + 1);
            }
            return *this;
        }

        inline chi_iterator operator++(int) {
            chi_iterator copy(*this);
            ++(*this);
            return copy;
        }
        /** @endcond */

        /** @brief Returns true if two iterators are equal.
         *
         * Two iterators are equal either if they're both past-the-end, or if they are bound to the same pool and point
         * to the same element.
         *
         * @param other Other element to compare to.
         */
        inline bool operator==(chi_iterator const &other) const {
            if (is_past_end()) {
                return other.is_past_end();
            } else {
                return _pool == other._pool and _i == other._i;
            }
        }

        /** @brief Negates @ref operator==.
         */
        inline bool operator!=(chi_iterator const &other) const {
            return not operator==(other);
        }
    };

    attachable_base::attachable_base() : _pool(nullptr) {}

    bool attachable_base::is_attached() const {
        return pool() != nullptr;
    }

    void attachable_base::after_attach() {}

    void attachable_base::before_detach() {}

    idx_pool_base const *attachable_base::pool() const {
        return _pool;
    }

    void attachable_base::attach_to(idx_pool_base const &pool) {
        pool.attach(*this);
    }

    void attachable_base::detach() {
        if (pool() != nullptr) {
            pool()->detach(*this);
        }
    }

    template<class T, class Ref, class CRef>
    typename storage_base<T, Ref, CRef>::const_reference storage_base<T, Ref, CRef>::at(std::size_t i) const {
        assert(pool() != nullptr);
        if (not pool()->is_allocated(i)) {
            throw std::out_of_range("The requested element is not allocated.");
        }
        return operator[](i);
    }

    template<class T, class Ref, class CRef>
    typename storage_base<T, Ref, CRef>::reference storage_base<T, Ref, CRef>::at(std::size_t i) {
        assert(pool() != nullptr);
        if (not pool()->is_allocated(i)) {
            throw std::out_of_range("The requested element is not allocated.");
        }
        return operator[](i);
    }

    void idx_pool_base::before_allocate(std::size_t idx) {
        _cap = std::max(_cap, idx + 1);
        ++_sz;
    }

    void idx_pool_base::after_destroy(std::size_t) {
        --_sz;
    }

    attachable_base::~attachable_base() {
        if (pool() != nullptr) {
            pool()->detach_internal(*this);
        }
    }

    idx_pool_base::~idx_pool_base() {
        while (not _attached.empty()) {
            _attached.front()->detach();
        }
    }

    chi_iterator idx_pool_base::begin() const {
        return {*this, false};
    }

    chi_iterator idx_pool_base::end() const {
        return {*this, true};
    }

    std::size_t idx_pool_base::get_first_allocated_entry(std::size_t begin) const {
        while (begin < capacity()) {
            if (is_allocated(begin)) {
                return begin;
            }
            ++begin;
        }
        return std::numeric_limits<std::size_t>::max();
    }

}

#endif //HE_STORAGE_BASE_H
