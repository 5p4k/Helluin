//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 06/08/16.
//

#include "storage_base.h"
#include <cassert>

using namespace he;

void idx_pool_base::attach(attachable_base &attachable) const {
    if (attachable.pool() == this) {
        return;
    } else if (attachable.is_attached()) {
        attachable.detach();
    }
    assert(attachable.pool() == nullptr);
    attachable._pool = this;
    attachable._self = _attached.insert(_attached.end(), &attachable);
    attachable.after_attach();
}

void idx_pool_base::detach(attachable_base &attachable) const {
    assert(attachable.pool() == this);
    attachable.before_detach();
    detach_internal(attachable);
}

void idx_pool_base::detach_internal(attachable_base &attachable) const {
    _attached.erase(attachable._self);
    attachable._self = std::list<attachable_base *>::iterator();
    attachable._pool = nullptr;
}

void idx_pool_base::destroy(std::size_t i) {
    assert(is_allocated(i));
    for (auto attachable : _attached) {
        attachable->destroy(i);
    }
    after_destroy(i);
}

std::size_t idx_pool_base::clone(std::size_t other) {
    assert(is_allocated(other));
    const std::size_t i = get_allocation_idx();
    assert(not is_allocated(i));
    before_allocate(i);
    for (auto attachable : _attached) {
        attachable->clone(i, other);
    }
    return i;
}

std::size_t idx_pool_base::allocate() {
    const std::size_t i = get_allocation_idx();
    assert(not is_allocated(i));
    before_allocate(i);
    for (auto attachable : _attached) {
        attachable->allocate(i);
    }
    return i;
}

std::size_t idx_pool_base::get_allocation_idx() const {
    for (std::size_t i = 0; i < size(); ++i) {
        if (not is_allocated(i)) {
            return i;
        }
    }
    return capacity();
}