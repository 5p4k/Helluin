//
//   Copyright 2017 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 02.09.17.
//
/** @file graph_base.h
 * @brief Basic definitions of arcs, nodes and directions.
 */

#ifndef HE_GRAPH_DECL_H
#define HE_GRAPH_DECL_H

/** @namespace he */
namespace he {

    /** @brief Base, empty arc structure, for declaring strongly typed `idx<arc>`.
     */
    struct arc {
    };

    /** @brief Base, empty node structure, for declaring strongly typed `idx<node>`.
     */
    struct node {
    };

    /** @brief Direction in which an arc is directed w.r.t. one of its adjacent nodes.
     */
    enum struct arc_dir : unsigned char {
        incoming = 0x1,     ///< An arc `(v, w)`, w.r.t. `w`, with `v != w`.
        loop = 0x2,         ///< An arc `(v, v)`, w.r.t. `v`.
        outgoing = 0x4,     ///< An arc `(v, w)`, w.r.t. `v`, with `v != w`.
        any_incoming = 0x3, ///< An arc `(v, w)`, w.r.t. `w` (possibly `v == w`).
        any_outgoing = 0x6, ///< An arc `(v, w)`, w.r.t. `v` (possibly `v == w`).
        any_no_loop = 0x5,  ///< An arc `(v, w)`, where `v != w`.
        any = 0x7,          ///< Any type of arc.
    };

    /** @brief Verifies if @p l matches @p r (or vice versa).
     *
     * For example, `loop & any_incoming == true`, `incoming & any == true`, `any_outgoing & incoming == false`.
     */
    inline static bool operator&(arc_dir l, arc_dir r);

    /** @brief Combines together two arc directions.
     *
     * For example, `(loop | incoming) == any_incoming`.
     * @note The enum @ref arc_dir is complete, that is, any combination of two values with this operator gives a
     * preexisting entry of the enum.
     */
    inline static arc_dir operator|(arc_dir l, arc_dir r);

    /** @brief Enum that describes the endpoints of an arc.
     */
    enum struct arc_endpt : unsigned char {
        tail,   ///< The tail of an arc, i.e. `v` for an arc `(v, w)`.
        head    ///< The head of an arc, i.e. `w` for an arc `(v, w)`.
    };


    bool operator&(arc_dir l, arc_dir r) {
        return static_cast<unsigned char>(l) == (static_cast<unsigned char>(l) & static_cast<unsigned char>(r)) or
               static_cast<unsigned char>(r) == (static_cast<unsigned char>(l) & static_cast<unsigned char>(r));
    }

    arc_dir operator|(arc_dir l, arc_dir r) {
        return static_cast<arc_dir>(static_cast<unsigned char>(l) | static_cast<unsigned char>(r));
    }

}

#endif //HE_GRAPH_DECL_H
