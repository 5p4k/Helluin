//
//   Copyright 2017 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 19.04.17.
//

#ifndef HE_NODE_PROXIES_H
#define HE_NODE_PROXIES_H

#include "graph_traits.h"

namespace he {

    namespace proxy {

        namespace impl {

            template<class G, class T>
            class const_proxy_base : public idx<T> {
            public:
                using idx_type = idx<T>;
                using graph_type = typename graph_traits<G>::graph_type;

                inline const_proxy_base() : idx_type(invalid_idx), _g(nullptr) {}

                inline const_proxy_base(graph_type const &g, idx_type i) : idx_type(i), _g(&g) {}

                inline graph_type const &graph() const {
                    return *_g;
                }

            private:
                graph_type const *_g;
            };

            template<class G, class T>
            class proxy_base : public virtual const_proxy_base<G, T> {
                using base_type = const_proxy_base<G, T>;
            public:
                using idx_type = typename base_type::idx_type;
                using graph_type = typename base_type::graph_type;

                inline proxy_base() = default;

                inline proxy_base(graph_type &g, idx_type i) : base_type(g, i) {}

                using base_type::graph;

                inline graph_type &
                graph() { return const_cast<graph_type &>(const_cast<proxy_base const *>(this)->graph()); }
            };

            template<class G, class It, class Jt>
            simple_range<Jt> remap_range(G &graph, simple_range<It> const &in_range) {
                return simple_range<Jt>(Jt(graph, in_range.begin()), Jt(graph, in_range.end()));
            }

        }

        template<class G>
        class node_proxy;

        template<class G>
        class node_const_proxy;

        template<class G>
        class arc_proxy;

        template<class G>
        class arc_const_proxy;

        template<class G>
        class delta_const_proxy_iterator :
                public target_remap_iterator_base<delta_const_proxy_iterator<G>, adj_arcs_iterator, typename std::add_const<G>::type> {
            using base_iterator = target_remap_iterator_base<delta_const_proxy_iterator, adj_arcs_iterator, typename std::add_const<G>::type>;
        public:
            using value_type = std::pair<arc_const_proxy<G>, arc_dir>;
            using reference = value_type;
            using pointer = void;
            using base_iterator::base_iterator;
            using base_iterator::inner;
            using base_iterator::target;

            inline std::pair<arc_const_proxy<G>, arc_dir> operator*() const {
                auto const temp = *inner();
                return std::pair<arc_const_proxy<G>, arc_dir>(
                        arc_const_proxy<G>(target(), temp.first), temp.second
                );
            }
        };

        template<class G>
        class delta_proxy_iterator :
                public target_remap_iterator_base<delta_proxy_iterator<G>, adj_arcs_iterator, G> {
            using base_iterator = target_remap_iterator_base<delta_proxy_iterator, adj_arcs_iterator, G>;
        public:
            using value_type = std::pair<arc_proxy<G>, arc_dir>;
            using reference = value_type;
            using pointer = void;
            using base_iterator::base_iterator;
            using base_iterator::inner;
            using base_iterator::target;

            inline std::pair<arc_const_proxy<G>, arc_dir> operator*() const {
                auto const temp = *inner();
                return std::pair<arc_const_proxy<G>, arc_dir>(
                        arc_const_proxy<G>(target(), temp.first), temp.second
                );
            }

            inline std::pair<arc_proxy<G>, arc_dir> operator*() {
                auto const temp = *inner();
                return std::pair<arc_proxy<G>, arc_dir>(
                        arc_proxy<G>(target(), temp.first), temp.second
                );
            }
        };

        template<class G>
        class gamma_const_proxy_iterator :
                public target_remap_iterator_base<gamma_const_proxy_iterator<G>, adj_arcs_iterator, typename std::add_const<G>::type> {
            using base_iterator = target_remap_iterator_base<gamma_const_proxy_iterator, adj_arcs_iterator, typename std::add_const<G>::type>;
        public:
            using value_type = node_const_proxy<G>;
            using reference = value_type;
            using pointer = void;
            using base_iterator::base_iterator;
            using base_iterator::inner;
            using base_iterator::target;

            inline node_const_proxy<G> operator*() const {
                return node_const_proxy<G>(target(), target().endpoint(*inner()));
            }
        };

        template<class G>
        class gamma_proxy_iterator :
                public target_remap_iterator_base<gamma_proxy_iterator<G>, adj_arcs_iterator, G> {
            using base_iterator = target_remap_iterator_base<gamma_proxy_iterator, adj_arcs_iterator, G>;
        public:
            using value_type = node_proxy<G>;
            using reference = value_type;
            using pointer = void;
            using base_iterator::base_iterator;
            using base_iterator::inner;
            using base_iterator::target;

            inline node_const_proxy<G> operator*() const {
                return node_const_proxy<G>(target(), target().endpoint(*inner()));
            }

            inline node_proxy<G> operator*() {
                return node_proxy<G>(target(), target().endpoint(*inner()));
            }
        };

        template<class G>
        using delta_const_proxy_range = simple_range<delta_const_proxy_iterator<G>>;

        template<class G>
        using delta_proxy_range = simple_range<delta_proxy_iterator<G>>;

        template<class G>
        using gamma_const_proxy_range = simple_range<gamma_const_proxy_iterator<G>>;

        template<class G>
        using gamma_proxy_range = simple_range<gamma_proxy_iterator<G>>;

        template<class G>
        class node_const_proxy
                : public virtual impl::const_proxy_base<G, node> {
            using base_type = impl::const_proxy_base<G, node>;
        public:
            using graph_type = typename base_type::graph_type;

            using node_type = typename graph_traits<G>::node_type;
            using node_const_reference = typename graph_traits<G>::node_const_reference;
            using node_const_pointer = typename graph_traits<G>::node_const_pointer;
            using node_reference = typename graph_traits<G>::node_reference;
            using node_pointer = typename graph_traits<G>::node_pointer;
            using node_proxy_type = node_proxy<G>;
            using node_const_proxy_type = node_const_proxy<G>;

            using arc_type = typename graph_traits<G>::arc_type;
            using arc_const_reference = typename graph_traits<G>::arc_const_reference;
            using arc_const_pointer = typename graph_traits<G>::arc_const_pointer;
            using arc_reference = typename graph_traits<G>::arc_reference;
            using arc_pointer = typename graph_traits<G>::arc_pointer;
            using arc_proxy_type = arc_proxy<G>;
            using arc_const_proxy_type = arc_const_proxy<G>;

            /// @brief Import all the methods, constructors included, of @ref const_proxy_base.
            /// @{
            inline node_const_proxy() = default;

            using base_type::graph;
            /// @}

            /// @brief Add a constructor which accepts a generic node index, if node_type is not void.
            inline node_const_proxy(graph_type const &g, idx<node> n) : base_type(g, n) {}

            /// @brief Add a constructor which implicitly casts a non const proxy of the same type
            inline node_const_proxy(node_proxy_type const &nonconst_proxy) : base_type(nonconst_proxy.graph(),
                                                                                       nonconst_proxy) {}

            /// @brief @returns True iff there is an allocated node behind this proxy.
            inline bool is_valid() const { return graph().node_pool().is_allocated(*this); }

            /// @brief @returns A const reference to the underlying payload.
            template<class T = node_const_reference, class = typename std::enable_if<not std::is_void<T>::value>::type>
            inline node_const_reference operator*() const { return graph()[*this]; }

            /// @brief @returns A const pointer to the underlying payload.
            template<class T = node_const_pointer, class = typename std::enable_if<not std::is_void<T>::value>::type>
            inline node_const_pointer operator->() const { return &operator*(); }


            inline node_const_proxy_type any_neighbor(arc_dir dir = arc_dir::any) const {
                return node_const_proxy_type(graph(), graph().any_neighbor(*this, dir));
            }

            inline arc_const_proxy_type any_adjacent_arc(arc_dir dir = arc_dir::any) const {
                return arc_const_proxy_type(graph(), graph().any_adjacent_arc(*this, dir));
            }

            inline weak_idx_t degree(arc_dir dir = arc_dir::any) const {
                return graph().degree(*this, dir);
            }

            inline node_const_proxy_type parent() const {
                return degree(arc_dir::incoming) == 1 ? any_neighbor(arc_dir::incoming) : node_const_proxy_type(graph(),
                                                                                                                invalid_idx);
            }

            inline bool is_adjacent(idx<node> n, bool directed = true) const {
                return graph().is_adjacent(*this, n, directed);
            }

            inline gamma_const_proxy_range<G> gamma(arc_dir dir = arc_dir::any) const {
                return impl::remap_range<const graph_type, adj_arcs_iterator, gamma_const_proxy_iterator<G>>(
                        graph(), graph().adjacent_arcs(*this, dir)
                );
            }

            inline delta_const_proxy_range<G> delta(arc_dir dir = arc_dir::any) const {
                return impl::remap_range<const graph_type, adj_arcs_iterator, delta_const_proxy_iterator<G>>(
                        graph(), graph().adjacent_arcs(*this, dir)
                );
            }
        };

        template<class G>
        class node_proxy : public impl::proxy_base<G, node>,
                           public node_const_proxy<G> {
            using root_base_type = impl::const_proxy_base<G, node>;
            using proxy_base_type = impl::proxy_base<G, node>;
            using node_base_type = node_const_proxy<G>;
        public:
            using graph_type = typename node_base_type::graph_type;

            using node_type = typename node_base_type::node_type;
            using node_const_reference = typename node_base_type::node_const_reference;
            using node_const_pointer = typename node_base_type::node_const_pointer;
            using node_reference = typename node_base_type::node_reference;
            using node_pointer = typename node_base_type::node_pointer;
            using node_proxy_type = typename node_base_type::node_proxy_type;
            using node_const_proxy_type = typename node_base_type::node_const_proxy_type;

            using arc_type = typename node_base_type::arc_type;
            using arc_const_reference = typename node_base_type::arc_const_reference;
            using arc_const_pointer = typename node_base_type::arc_const_pointer;
            using arc_reference = typename node_base_type::arc_reference;
            using arc_pointer = typename node_base_type::arc_pointer;
            using arc_proxy_type = typename node_base_type::arc_proxy_type;
            using arc_const_proxy_type = typename node_base_type::arc_const_proxy_type;

            /// @brief Import all the methods, constructors included, of the base classes.
            /// @{
            inline node_proxy() = default;

            using proxy_base_type::graph;
            using node_base_type::is_valid;
            using node_base_type::operator*;
            using node_base_type::operator->;
            using node_base_type::any_neighbor;
            using node_base_type::any_adjacent_arc;
            using node_base_type::degree;
            using node_base_type::parent;
            using node_base_type::is_adjacent;
            using node_base_type::gamma;
            using node_base_type::delta;
            /// @}

            /// @brief Add a constructor which accepts a generic node index, if node_type is not void.
            inline node_proxy(graph_type const &g, idx<node> n) : root_base_type(g, n) {}

            /// @brief @returns A reference to the underlying payload.
            template<class T = node_reference, class = typename std::enable_if<not std::is_void<T>::value>::type>
            inline node_reference operator*() { return graph()[*this]; }

            /// @brief @returns A pointer to the underlying payload.
            template<class T = node_pointer, class = typename std::enable_if<not std::is_void<T>::value>::type>
            inline node_pointer operator->() { return &operator*(); }


            inline node_proxy_type any_neighbor(arc_dir dir = arc_dir::any) {
                return node_proxy_type(graph(), graph().any_neighbor(*this, dir));
            }

            inline arc_proxy_type any_adjacent_arc(arc_dir dir = arc_dir::any) {
                return arc_proxy_type(graph(), graph().any_adjacent_arc(*this, dir));
            }

            inline weak_idx_t degree(arc_dir dir = arc_dir::any) {
                return graph().degree(*this, dir);
            }

            inline node_proxy_type parent() {
                return degree(arc_dir::incoming) == 1 ? any_neighbor(arc_dir::incoming) : node_proxy_type(graph(),
                                                                                                          invalid_idx);
            }

            inline gamma_proxy_range<G> gamma(arc_dir dir = arc_dir::any) {
                return impl::remap_range<graph_type, adj_arcs_iterator, gamma_proxy_iterator<G>>(
                        graph(), graph().adjacent_arcs(*this, dir)
                );
            }

            inline delta_proxy_range<G> delta(arc_dir dir = arc_dir::any) {
                return impl::remap_range<graph_type, adj_arcs_iterator, delta_proxy_iterator<G>>(
                        graph(), graph().adjacent_arcs(*this, dir)
                );
            }

            inline void erase() {
                graph().erase(*this);
            }
        };

        template<class G>
        class arc_const_proxy
                : public virtual impl::const_proxy_base<G, arc> {
            using base_type = impl::const_proxy_base<G, arc>;
        public:
            using graph_type = typename base_type::graph_type;

            using node_type = typename graph_traits<G>::node_type;
            using node_const_reference = typename graph_traits<G>::node_const_reference;
            using node_const_pointer = typename graph_traits<G>::node_const_pointer;
            using node_reference = typename graph_traits<G>::node_reference;
            using node_pointer = typename graph_traits<G>::node_pointer;
            using node_proxy_type = node_proxy<G>;
            using node_const_proxy_type = node_const_proxy<G>;

            using arc_type = typename graph_traits<G>::arc_type;
            using arc_const_reference = typename graph_traits<G>::arc_const_reference;
            using arc_const_pointer = typename graph_traits<G>::arc_const_pointer;
            using arc_reference = typename graph_traits<G>::arc_reference;
            using arc_pointer = typename graph_traits<G>::arc_pointer;
            using arc_proxy_type = arc_proxy<G>;
            using arc_const_proxy_type = arc_const_proxy<G>;

            /// @brief Import all the methods, constructors included, of the base classes.
            /// @{
            inline arc_const_proxy() = default;

            using base_type::graph;
            /// @}

            /// @brief Add a constructor which accepts a generic arc index, if arc_type is not void.
            inline arc_const_proxy(graph_type const &g, idx<arc> a) : base_type(g, a) {}

            /// @brief Add a constructor which implicitly casts a non const proxy of the same type
            inline arc_const_proxy(arc_proxy_type const &nonconst_proxy) : base_type(nonconst_proxy.graph(),
                                                                                     nonconst_proxy) {}

            /// @brief @returns True iff there is an allocated arc behind this proxy.
            inline bool is_valid() const { return graph().arc_pool().is_allocated(*this); }

            /// @brief @returns A const reference to the underlying payload.
            template<class T = arc_const_reference, class = typename std::enable_if<not std::is_void<T>::value>::type>
            inline arc_const_reference operator*() const { return graph()[*this]; }

            /// @brief @returns A const pointer to the underlying payload.
            template<class T = arc_const_pointer, class = typename std::enable_if<not std::is_void<T>::value>::type>
            inline arc_const_pointer operator->() const { return &operator*(); }

            /// @brief Access head and tail.
            /// @{
            inline node_const_proxy_type head() const {
                return node_const_proxy_type(graph(), graph().endpoint(*this, arc_endpt::head));
            }

            inline node_const_proxy_type tail() const {
                return node_const_proxy_type(graph(), graph().endpoint(*this, arc_endpt::tail));
            }

            inline node_const_proxy_type operator[](arc_dir dir) const {
                return node_const_proxy_type(graph(), graph().endpoint(*this, dir));
            }

            inline node_const_proxy_type operator[](arc_endpt endpt) const {
                return node_const_proxy_type(graph(), graph().endpoint(*this, endpt));
            }
            /// @}
        };


        template<class G>
        class arc_proxy : public impl::proxy_base<G, arc>,
                          public arc_const_proxy<G> {
            using root_base_type = impl::const_proxy_base<G, arc>;
            using proxy_base_type = impl::proxy_base<G, arc>;
            using arc_base_type = arc_const_proxy<G>;
        public:
            using graph_type = typename arc_base_type::graph_type;

            using node_type = typename arc_base_type::node_type;
            using node_const_reference = typename arc_base_type::node_const_reference;
            using node_const_pointer = typename arc_base_type::node_const_pointer;
            using node_reference = typename arc_base_type::node_reference;
            using node_pointer = typename arc_base_type::node_pointer;
            using node_proxy_type = typename arc_base_type::node_proxy_type;
            using node_const_proxy_type = typename arc_base_type::node_const_proxy_type;

            using arc_type = typename arc_base_type::arc_type;
            using arc_const_reference = typename arc_base_type::arc_const_reference;
            using arc_const_pointer = typename arc_base_type::arc_const_pointer;
            using arc_reference = typename arc_base_type::arc_reference;
            using arc_pointer = typename arc_base_type::arc_pointer;
            using arc_proxy_type = typename arc_base_type::arc_proxy_type;
            using arc_const_proxy_type = typename arc_base_type::arc_const_proxy_type;

            /// @brief Import all the methods, constructors included, of the base classes.
            /// @{
            inline arc_proxy() = default;

            using proxy_base_type::graph;
            using arc_base_type::is_valid;
            using arc_base_type::operator*;
            using arc_base_type::operator->;
            using arc_base_type::head;
            using arc_base_type::tail;
            /// @}

            /// @brief Add a constructor which accepts a generic arc index, if arc_type is not void.
            inline arc_proxy(graph_type const &g, idx<arc> a) : root_base_type(g, a) {}

            /// @brief @returns A reference to the underlying payload.
            template<class T = arc_reference, class = typename std::enable_if<not std::is_void<T>::value>::type>
            inline arc_reference operator*() { return graph()[*this]; }

            /// @brief @returns A pointer to the underlying payload.
            template<class T = arc_pointer, class = typename std::enable_if<not std::is_void<T>::value>::type>
            inline arc_pointer operator->() { return &operator*(); }

            /// @brief Access head and tail, as writeable objects.
            /// @{
            inline node_proxy_type head() { return node_proxy_type(graph(), graph().endpoint(*this, arc_endpt::head)); }

            inline node_proxy_type tail() { return node_proxy_type(graph(), graph().endpoint(*this, arc_endpt::tail)); }

            inline node_proxy_type operator[](arc_dir dir) {
                return node_proxy_type(graph(), graph().endpoint(*this, dir));
            }

            inline node_proxy_type operator[](arc_endpt endpt) {
                return node_proxy_type(graph(), graph().endpoint(*this, endpt));
            }
            /// @}

            /// @brief Change head or tail.
            /// @{
            inline void set_head(idx<node> new_head) { graph().change_arc(*this, invalid_idx, new_head); }

            inline void set_tail(idx<node> new_tail) { graph().change_arc(*this, new_tail, invalid_idx); }

            inline void set(arc_endpt endpt, idx<node> new_endpt) {
                if (endpt == arc_endpt::head) {
                    set_head(new_endpt);
                } else {
                    set_tail(new_endpt);
                }
            }

            inline void set(idx<node> new_tail, idx<node> new_head) {
                graph().change(*this, new_tail, new_head);
            }
            /// @}

            inline void erase() {
                graph().erase(*this);
            }
        };
    }

    template<class G,
            class NConstProxy = typename proxy::node_const_proxy<G>,
            class AConstProxy = typename proxy::arc_const_proxy<G>,
            class NProxy = typename proxy::node_proxy<G>,
            class AProxy = typename proxy::arc_proxy<G>
    >
    class proxied : public G {
    public:
        using G::G;
        using node_proxy = NProxy;
        using node_const_proxy = NConstProxy;
        using arc_proxy = AProxy;
        using arc_const_proxy = AConstProxy;

        inline arc_const_proxy proxy(idx<arc> i) const { return arc_const_proxy(*this, i); }

        inline arc_proxy proxy(idx<arc> i) { return arc_proxy(*this, i); }

        inline node_const_proxy proxy(idx<node> i) const { return node_const_proxy(*this, i); }

        inline node_proxy proxy(idx<node> i) { return node_proxy(*this, i); }
    };

    template<class... Args>
    using proxied_graph = proxied<graph<Args...>>;
}

#endif //HE_NODE_PROXIES_H
