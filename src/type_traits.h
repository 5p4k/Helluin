//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 25/08/16.
//
/** @file type_traits.h
 * @brief Some extra type traits for helping with SFINAE expression.
 */

#ifndef HE_TYPE_TRAITS_H
#define HE_TYPE_TRAITS_H

#include <type_traits>
#include <iterator>

/** @namespace he */
namespace he {

    /** @brief Voids all the template parameters.
     */
    template<class...>
    using void_type = void;

    /** @brief Default implementation for `all_true` template paramters. Thit is by default false.
     * There are implementation for the case of a single true parameter at the head of the template parameter lists
     * which discharge the truth assertion on the following parameters recursively. Specifying false anywhere, will
     * resolve to this one declaration.
     */
    template<bool...>
    struct all_true : std::false_type {
    };

    /** @brief Partial implementation for `all_true` template parameters. One parameter, true implies truth value.
     */
    template<>
    struct all_true<true> : std::true_type {
    };

    /** @brief Partial implementaiton for `all_true` template parameters: first parameter is true, inherit recursively.
     */
    template<bool... Args>
    struct all_true<true, Args...> : all_true<Args...> {
    };

    /** @cond */
    namespace trait_test {
        /* These decltype expression have the effect of instantiating a certain expression, and will be defined if and
         * only if this expression makes sense. In combination with `well_formed`, they can yield a true/false constexpr
         * describing whether that expression makes sense or not without causing a compilation error.
         */
        template<class T>
        using postfix_increment = decltype(std::declval<T &>()++);

        template<class T>
        using postfix_decrement = decltype(std::declval<T &>()--);

        template<class T>
        using prefix_increment = decltype(++std::declval<T &>());

        template<class T>
        using prefix_decrement = decltype(--std::declval<T &>());

        template<class T, class DiffT>
        using n_increment = decltype(std::declval<T &>() += std::declval<DiffT>());

        template<class T, class DiffT>
        using n_decrement = decltype(std::declval<T &>() -= std::declval<DiffT>());

        template<class T, class U = T>
        using differentiable = decltype(std::declval<T>() - std::declval<U>());

        template<class T, class U>
        using equatable = decltype(std::declval<T const &>() == std::declval<U const &>());

        template<class T, class U>
        using inequatable = decltype(std::declval<T const &>() != std::declval<U const &>());

        template<class T>
        using range_iterable = decltype(++std::begin(std::declval<T &>()) != std::end(std::declval<T &>().end()));

        template<class T>
        using address_of_able = decltype(&std::declval<T>());
    }

    namespace impl {
        /* The trick behind `well_formed`, is that the expression that we want to check is expressed via a template.
         * Therefore it is evaluate in the template resolution context, and if it fails, SFINAE will cause it to fall
         * back on the default false constexpr. First it will try the more specialized version, where the argument is
         * voided by `void_type`, and then fall back on the more generic.
         */

        template<class AlwaysVoid, template<class...> class Test, class... Args>
        struct well_formed : std::false_type {
        };

        template<template<class...> class Test, class... Args>
        struct well_formed<void_type<Test<Args...>>, Test, Args...> : std::true_type {
        };

        template<class T, class U, bool>
        struct difference_type {
            using type = void;
            static constexpr bool valid = false;
        };

        template<class T, class U>
        struct difference_type<T, U, true> {
            using type = decltype(std::declval<T>() - std::declval<U>());
            static constexpr bool valid = true;
        };

        template<class, bool>
        struct address_of_type {
            using type = void;
            static constexpr bool valid = false;
        };

        template<class T>
        struct address_of_type<T, true> {
            using type = decltype(&std::declval<T>());
            static constexpr bool valid = true;
        };

        template<class T, bool>
        struct range_iterable_type {
            using iterator = void;
            using const_iterator = void;
            static constexpr bool valid = false;
        };

        template<class T>
        struct range_iterable_type<T, true> {
            using iterator = decltype(std::begin(std::declval<T &>()));
            using const_iterator = decltype(std::begin(std::declval<T const &>()));
            static constexpr bool valid = true;
        };
    }
    /** @endcond */


    /** @brief Boolean constexpr expressing whether `Test<Args...>` is defined.
     */
    template<template<class...> class Test, class... Args>
    using is_detected = typename impl::well_formed<void, Test, Args...>;

    /** @brief Trait expressing whether it is valid to take the address of an object of type `T` via `&`.
     */
    template<class T>
    using is_address_of_able = is_detected<trait_test::address_of_able, T>;

    /** @brief Trait expressing the type resulting from taking the address of an object of type `T` via `&`.
     */
    template<class T>
    using address_of_type = impl::address_of_type<T, is_address_of_able<T>::value>;

    /** @brief Trait expressing whether the prefix and postfix increment `++` operator make sense on an instance of `T`.
     */
    template<class T>
    using is_incrementable = all_true<
            is_detected<trait_test::prefix_increment, T>::value,
            is_detected<trait_test::postfix_increment, T>::value
    >;

    /** @brief Trait expressing whether the prefix and postfix decrement `--` operator make sense on an instance of `T`.
     */
    template<class T>
    using is_decrementable = all_true<
            is_detected<trait_test::prefix_decrement, T>::value,
            is_detected<trait_test::postfix_decrement, T>::value
    >;

    /** @brief Trait expressing whether it is possible to use the `+=` operator on an instance of `T` and `DiffT`.
     */
    template<class T, class DiffT>
    using is_n_incrementable = is_detected<trait_test::n_increment, T, DiffT>;

    /** @brief Trait expressing whether it is possible to use the `-=` operator on an instance of `T` and `DiffT`.
     */
    template<class T, class DiffT>
    using is_n_decrementable = is_detected<trait_test::n_decrement, T, DiffT>;

    /** @brief Trait expressing whether it is possible to use operator `==` on an instance of `T` and `U`.
     */
    template<class T, class U>
    using is_equatable = is_detected<trait_test::equatable, T, U>;

    /** @brief Trait expressing whether it is possible to use operator `!=` on an instance of `T` and `U`.
     */
    template<class T, class U>
    using is_inequatable = is_detected<trait_test::inequatable, T, U>;

    /** @brief Trait expressing whether it is possible to use operator `-` on an instance of `T` and `U`.
     */
    template<class T, class U = T>
    using is_differentiable = is_detected<trait_test::differentiable, T, U>;

    /** @brief Trait expressing the resulting type of using operator `==` on an instance of `T` and `U`.
     */
    template<class T, class U>
    using difference_type = impl::difference_type<T, U, is_differentiable<T, U>::value>;

    /** @brief Trait expressing whether it is possible to convert to `DiffT` the result of operator `-` on an instance
     * of `T` and `U`.
     */
    template<class T, class U = T, class DiffT = typename T::difference_type>
    using is_differentiable_as = all_true<
            is_differentiable<T, U>::value,
            std::is_convertible<typename difference_type<T, U>::type, DiffT>::value
    >;

    /** @brief Trait expressing whether it is possible to range-iterator over `T`.
     * As a matter of fact, it checks whether the following expression is well-formed:
     * @code
     * ++std::begin(std::declval<T &>()) != std::end(std::declval<T &>().end())
     * @endcode
     */
    template<class T>
    using is_range_iterable = is_detected<trait_test::range_iterable, T>;

    /** @brief Trait expressing the type that a range iteration over `T` would yield.
     */
    template<class T>
    using range_iterable_type = impl::range_iterable_type<T, is_range_iterable<T>::value>;

    /** @cond */
    namespace impl {
        template<class T, class U, bool> // bool = T is smaller than U
        struct saturate_cast {
            inline static T cast(U value) {
                if (value >= static_cast<U>(std::numeric_limits<T>::max())) {
                    throw std::out_of_range("Unsigned integer overflow.");
                }
                return static_cast<T>(value);
            }
        };

        template<class T, class U>
        struct saturate_cast<T, U, false> {
            inline static T cast(U value) {
                return static_cast<T>(value);
            }
        };

        template<class T, class U, class = typename std::enable_if<
                std::is_unsigned<T>::value and std::is_unsigned<U>::value>::type>
        struct cast_could_saturate : public std::integral_constant<bool,
                std::numeric_limits<T>::max() < std::numeric_limits<U>::max()> {
        };
    }
    /** @endcond */

    /** @brief Function that casts two unsigned integer types `U` to `T`, and throws if the casts causes overflow.
     *
     * If `T` is such that casting from `U` cannot cause integer overflow (condition asserted basing on the fact that
     * the `std::numeric_limits::max()` value of one is bigger than the other), this is equivalent to a `static_cast`.
     * Otherwise, this is a `static_cast` with an extra check for turning integer overflow into an exception.
     *
     * @tparam T Type to convert to.
     * @tparam U Type to convert from.
     * @param value Value to convert.
     * @return The result of `static_cast` from `U` to `T` applied to @p value.
     */
    template<class T, class U>
    inline T saturate_cast(U value) {
        return impl::saturate_cast<T, U, impl::cast_could_saturate<T, U>::value>::cast(value);
    }
}

#endif //HE_TYPE_TRAITS_H
