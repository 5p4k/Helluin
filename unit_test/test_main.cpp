//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 23/09/16.
//

#define CATCH_CONFIG_MAIN

#include <catch.hpp>


struct CoverageReporter : public Catch::ConsoleReporter {
    CoverageReporter(Catch::ReporterConfig const &config) : Catch::ConsoleReporter(config) {}

    static std::string getDescription() {
        return "As the 'console' reporter, but prints coverage percentage.";
    }

    void testRunEnded(Catch::TestRunStats const &stats) override {
        Catch::ConsoleReporter::testRunEnded(stats);

        // Print regexp-friendly message at the end.
        const double coverage = double(stats.totals.assertions.passed) / double(stats.totals.assertions.total());
        stream << int(100. * coverage) << "% tests passed" << std::endl;
    }
};

INTERNAL_CATCH_REGISTER_REPORTER("coverage", CoverageReporter)
