//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 12/11/2016.
//

#include "catch_nowarn.h"
#include <graph_adj_list.h>
#include <algo.h>
#include "utils.h"

using namespace he;

TEST_CASE("Checking that quick_dfs crawls exactly once every vertex.", "[algo][dfs]") {
    adj_list_graph<> g;
    REQUIRE_NOTHROW(populate_graph(g, 10, 10));

    storage<unsigned> visit_count(0);
    visit_count.attach_to(g.node_pool());

    for (std::size_t i : g.node_pool()) {
        if (visit_count[i] == 0) {
            // Part of a connected component which was not visited.
            quick_dfs(g, g.node_at(i), [&](graph_base const &, idx<node> j)
            {
                REQUIRE(visit_count[j] == 0);
                ++visit_count[j];
            });
            REQUIRE(visit_count[i] == 1);
        }
    }

    for (unsigned count : visit_count) {
        REQUIRE(count == 1);
    }
}