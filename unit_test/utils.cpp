//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 12/11/2016.
//

#include <random>
#include "utils.h"

void populate_graph(graph_base &g, unsigned n, unsigned m, bool allow_loops) {
    for (unsigned i = 0; i < n; ++i) {
        g.emplace_node();
    }
    std::mt19937 gen(some_seed);
    std::uniform_int_distribution<unsigned> a_node(0, n - 1);
    for (unsigned j = 0; j < m; ++j) {
        idx<node> n_head = g.node_at(a_node(gen));
        unsigned tail_idx;
        do {
            tail_idx = a_node(gen);
        }
        while (not allow_loops and tail_idx == n_head);
        idx<node> n_tail = g.node_at(tail_idx);
        g.emplace_arc(n_tail, n_head);
    }
}
