//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 12/11/2016.
//

#ifndef HELLUIN_UTILS_H
#define HELLUIN_UTILS_H

#include <graph_base.h>
#include <graph_adj_list.h>

// Taken at 01:33 AM CEST, in a cold autumn night.
static constexpr unsigned some_seed = 1578860658u;

using namespace he;

void populate_graph(graph_base &g, unsigned n = 10, unsigned m = 10, bool allow_loops = true);

#endif //HELLUIN_UTILS_H
