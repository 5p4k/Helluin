//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 06/10/16.
//

#include <random>
#include "catch_nowarn.h"
#include <graph_adj_list.h>
#include <proxies.h>
#include "utils.h"

using namespace he;

TEST_CASE("Checking that idx can be initialized correctly.", "[idx]") {
    idx<int> test1;
    REQUIRE(test1 == invalid_idx);
    REQUIRE(weak_idx_t(test1) == weak_idx_t(invalid_idx));
    idx<int> test2 = invalid_idx;
    REQUIRE(test2 == invalid_idx);
    REQUIRE(weak_idx_t(test2) == weak_idx_t(invalid_idx));
    idx<int> test3(invalid_idx);
    REQUIRE(test3 == invalid_idx);
    REQUIRE(weak_idx_t(test3) == weak_idx_t(invalid_idx));
    REQUIRE(!test1);
    REQUIRE(!invalid_idx);
}

TEST_CASE("Base graph building with no payload", "[graph_base][graph_adj_list][proxy]") {
    static constexpr unsigned n = 10;
    std::mt19937 gen(some_seed);
    std::uniform_int_distribution<unsigned> a_node(0, n - 1);

    adj_list_graph<> g;

    for (unsigned i = 0; i < n; ++i) {
        idx<node> inode = invalid_idx;
        REQUIRE(!inode);
        REQUIRE_NOTHROW(inode = g.emplace_node());
        REQUIRE(inode);
        REQUIRE(inode == weak_idx_t(i));
        REQUIRE(g.node_pool().size() == i + 1);
    }

    std::vector<std::pair<idx<node>, idx<node>>> arcs;

    // Add 10 edges. This is intended to be tested, that's why we don't use an helper function.
    for (unsigned i = 0; i < n; ++i) {
        idx<node> head = invalid_idx;
        idx<node> tail = invalid_idx;
        REQUIRE(!head);
        REQUIRE(!tail);
        REQUIRE_NOTHROW(head = g.node_at(a_node(gen)));
        REQUIRE_NOTHROW(tail = g.node_at(a_node(gen)));
        REQUIRE(head != invalid_idx);
        REQUIRE(tail != invalid_idx);
        REQUIRE(head);
        REQUIRE(tail);
        REQUIRE(g.node_pool().is_allocated(head));
        REQUIRE(g.node_pool().is_allocated(tail));
        idx<arc> new_arc;
        REQUIRE(!new_arc);
        REQUIRE_NOTHROW(new_arc = g.emplace_arc(tail, head));
        REQUIRE(new_arc);
        arcs.emplace_back(tail, head);
    }

    REQUIRE(g.arc_pool().size() == n);

    SECTION("Adjacency and degree test") {
        // Adjacency test
        for (auto const &ht : arcs) {
            if (not g.is_adjacent(ht.first, ht.second)) {
                bool retval = g.is_adjacent(ht.first, ht.second);
                printf("WAT%s\n", retval ? "y" : "n");
            }
            REQUIRE(g.is_adjacent(ht.first, ht.second));
            // If this is a loop it will work also reversed.
            if (ht.first != ht.second) {
                REQUIRE(!g.is_adjacent(ht.second, ht.first, true));
            }
        }

        // Degree test
        std::array<unsigned, n> in_degree;
        std::array<unsigned, n> out_degree;
        std::array<unsigned, n> loops_degree;
        std::fill(in_degree.begin(), in_degree.end(), 0u);
        std::fill(out_degree.begin(), out_degree.end(), 0u);
        std::fill(loops_degree.begin(), loops_degree.end(), 0u);
        for (auto const &ht : arcs) {
            in_degree[ht.second]++;
            out_degree[ht.first]++;
            if (ht.first == ht.second) {
                loops_degree[ht.first]++;
            }
        }
        for (unsigned i = 0; i < n; ++i) {
            REQUIRE(g.node_at(i));
            REQUIRE(g.degree(g.node_at(i), arc_dir::any_incoming) == in_degree[i]);
            REQUIRE(g.degree(g.node_at(i), arc_dir::any_outgoing) == out_degree[i]);
            REQUIRE(g.degree(g.node_at(i), arc_dir::any) == out_degree[i] + in_degree[i] - loops_degree[i]);
            using pnode = proxy::node_const_proxy<adj_list_graph<>>;
            pnode mi(g, g.node_at(i));
            REQUIRE(mi.degree(arc_dir::any_incoming) == in_degree[i]);
            REQUIRE(mi.degree(arc_dir::any_outgoing) == out_degree[i]);
            REQUIRE(mi.degree(arc_dir::any) == out_degree[i] + in_degree[i] - loops_degree[i]);
        }
    }

    SECTION("Proxy arc test") {
        using pnode = proxy::node_const_proxy<adj_list_graph<>>;

        std::vector<pnode> pnodes;
        for (std::size_t node_idx : g.node_pool()) {
            idx<node> node;
            REQUIRE_NOTHROW(node = g.node_at(node_idx));
            REQUIRE(node);
            pnode node_const_proxy(g, node);
            pnodes.push_back(node_const_proxy);
        }

        // Perform the same tests via proxy node
        for (auto const &ht : arcs) {
            auto const &ptail = pnodes[ht.first];
            auto const &phead = pnodes[ht.second];
            REQUIRE(phead.is_adjacent(ptail, false));
            REQUIRE(ptail.is_adjacent(phead, false));
            if (ht.first == ht.second) {
                REQUIRE(phead.is_adjacent(ptail, true));
            } else {
                REQUIRE(!phead.is_adjacent(ptail, true));
            }
            REQUIRE(ptail.is_adjacent(phead, true));
        }
    }
}

TEST_CASE("Loops", "[graph_base][graph_adj_list]") {
    adj_list_graph<> g;
    idx<node> v = g.emplace_node();
    idx<arc> e = g.emplace_arc(v, v);
    REQUIRE(g.arc_pool().size() == 1);
    for (auto element : g.arc_pool()) {
        REQUIRE(element == e);
    }
    REQUIRE(g.endpoint(e, arc_endpt::head) == v);
    REQUIRE(g.endpoint(e, arc_endpt::tail) == v);
    REQUIRE(g.is_adjacent(v, v, true));
    REQUIRE(g.is_adjacent(v, v, false));
    unsigned count_incoming = 0;
    unsigned count_outgoing = 0;
    unsigned count_any_incoming = 0;
    unsigned count_any_outgoing = 0;
    unsigned count_any = 0;
    for (auto item : g.adjacent_arcs(v, arc_dir::incoming)) {
        REQUIRE(g.endpoint(item) == v);
        ++count_incoming;
    }
    for (auto item : g.adjacent_arcs(v, arc_dir::outgoing)) {
        REQUIRE(g.endpoint(item) == v);
        ++count_outgoing;
    }
    for (auto item : g.adjacent_arcs(v, arc_dir::any_incoming)) {
        REQUIRE(g.endpoint(item) == v);
        ++count_any_incoming;
    }
    for (auto item : g.adjacent_arcs(v, arc_dir::any_outgoing)) {
        REQUIRE(g.endpoint(item) == v);
        ++count_any_outgoing;
    }
    for (auto item : g.adjacent_arcs(v, arc_dir::any)) {
        REQUIRE(g.endpoint(item) == v);
        ++count_any;
    }
    for (auto item : g.adjacent_arcs(v, arc_dir::any_no_loop)) {
        REQUIRE(item.second != arc_dir::loop);
    }
    REQUIRE(count_any == 1);
    REQUIRE(count_incoming == 0);
    REQUIRE(count_outgoing == 0);
    REQUIRE(count_any_incoming == 1);
    REQUIRE(count_any_outgoing == 1);
}

TEST_CASE("Adjacent node enumeration", "[graph_base][graph_adj_list][proxy]") {
    adj_list_graph<> g;
    REQUIRE_NOTHROW(populate_graph(g, 10, 10));

    SECTION("Classical enumeration") {
        for (std::size_t i : g.node_pool()) {
            idx<node> n = g.node_at(i);
            for (arc_dir dir : {arc_dir::incoming, arc_dir::outgoing, arc_dir::any}) {
                auto range = g.adjacent_arcs(n, dir);
                unsigned n_adj_arcs = 0;
                for (auto it = range.begin(); it != range.end(); ++it) {
                    REQUIRE_NOTHROW(g.degree(n, dir));
                    REQUIRE(g.degree(n, dir) > n_adj_arcs++);
                    auto adj_arc_node_pair = *it;
                    REQUIRE(adj_arc_node_pair.first);
                    auto const endpts = g.endpoints(adj_arc_node_pair.first);
                    if (endpts.first != endpts.second) {
                        // Not a loop
                        REQUIRE((dir & adj_arc_node_pair.second));
                    }
                    REQUIRE_NOTHROW(g.endpoint(adj_arc_node_pair));
                    REQUIRE(g.endpoint(adj_arc_node_pair));
                }
            }
        }
    }

    SECTION("Proxy enumeration") {
        using pnode = proxy::node_const_proxy<adj_list_graph<>>;
        for (std::size_t i : g.node_pool()) {
            pnode n(g, g.node_at(i));
            for (arc_dir dir : {arc_dir::incoming, arc_dir::outgoing, arc_dir::any}) {
                unsigned n_adj_arcs = 0;
                for (auto a : n.delta(dir)) {
                    REQUIRE(idx<arc>(a.first));
                    REQUIRE(n.degree(dir) > n_adj_arcs++);
                }
            }
        }
    }
}

TEST_CASE("Edge and node deletion", "[graph_base][graph_adj_list]") {
    adj_list_graph<> g;
    REQUIRE_NOTHROW(populate_graph(g, 12, 12, false));

    while (g.arc_pool().size() > 6) {
        auto a = g.any_arc();
        REQUIRE(g.arc_pool().is_allocated(a));
        const std::size_t old_arc_size = g.arc_pool().size();
        const auto endpoints = g.endpoints(a);
        const std::pair<weak_idx_t, weak_idx_t> old_degree{g.degree(endpoints.first, arc_dir::any),
                                                           g.degree(endpoints.second, arc_dir::any)};
        g.erase(a);
        REQUIRE(not g.arc_pool().is_allocated(a));
        REQUIRE(g.arc_pool().size() == old_arc_size - 1);
        for (auto b : g.adjacent_arcs(endpoints.first, arc_dir::any)) {
            REQUIRE(b.first != a);
        }
        for (auto b : g.adjacent_arcs(endpoints.second, arc_dir::any)) {
            REQUIRE(b.first != a);
        }
        REQUIRE(g.degree(endpoints.first, arc_dir::any) < old_degree.first);
        REQUIRE(g.degree(endpoints.second, arc_dir::any) < old_degree.second);
    }

    while (g.node_pool().size() > 0) {
        auto n = g.any_node();
        REQUIRE(g.node_pool().is_allocated(n));
        const std::size_t old_node_size = g.node_pool().size();
        const std::size_t old_arc_size = g.arc_pool().size();
        const bool has_arcs = g.degree(n, arc_dir::any) > 0;
        g.erase(n);
        REQUIRE(not g.node_pool().is_allocated(n));
        REQUIRE(g.node_pool().size() == old_node_size - 1);
        if (has_arcs) {
            REQUIRE(g.arc_pool().size() < old_arc_size);
        } else {
            REQUIRE(g.arc_pool().size() == old_arc_size);
        }
        for (auto i : g.arc_pool()) {
            std::pair<idx<node>, idx<node>> endpoints;
            REQUIRE_NOTHROW(endpoints = g.endpoints(g.arc_at(i)));
            REQUIRE(endpoints.first != n);
            REQUIRE(endpoints.second != n);
        }
    }
}

TEST_CASE("Changing edge endpoints", "[graph_base][graph_adj_list]") {
    adj_list_graph<> g;
    adj_list_graph<> h;

    populate_graph(g);
    populate_graph(h);

    // Transform h into g
    for (auto it : g.arc_pool()) {
        auto a = g.arc_at(it);
        auto const endpts = g.endpoints(a);

        REQUIRE_NOTHROW(h.change_arc(a, endpts.first, endpts.second));
        REQUIRE(h.endpoints(a) == endpts);
        bool found = false;
        for (auto b : h.adjacent_arcs(endpts.first)) {
            if (b.first == a) {
                found = true;
                break;
            }
        }
        REQUIRE(found);
        found = false;
        for (auto b : h.adjacent_arcs(endpts.second)) {
            if (b.first == a) {
                found = true;
                break;
            }
        }
        REQUIRE(found);
    }

    // Destroy all the elements. Destroying them triggers some checks in the adj_list
    while (h.node_pool().size() > 0) {
        REQUIRE_NOTHROW(h.erase(h.any_node()));
    }
}