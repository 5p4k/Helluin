//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 12/11/2016.
//

#include "catch_nowarn.h"
#include <adj_list.h>

using namespace he;

namespace {
    using content = std::pair<std::size_t, arc_dir>;

    class my_list : public adj_list<content> {
    protected:
        void update_reference(arc_dir type, content &item, weak_idx_t new_ref) override {
            REQUIRE(item.second == type);
            item.first = new_ref;
        }
    };
}

TEST_CASE("Testing adjacency list", "[graph_adj_list]") {
    my_list al;

    auto make = [](arc_dir t) -> content
    {
        return std::make_pair(std::numeric_limits<std::size_t>::max(), t);
    };

    SECTION("Populate with incoming arcs") {
        for (unsigned i = 0; i < 10; ++i) {
            al.push(arc_dir::incoming, make(arc_dir::incoming));
        }
    }

    SECTION("Populate with outgoing arcs") {
        for (unsigned i = 0; i < 10; ++i) {
            al.push(arc_dir::outgoing, make(arc_dir::outgoing));
        }
    }

    SECTION("Populate with loops") {
        for (unsigned i = 0; i < 10; ++i) {
            al.push(arc_dir::loop, make(arc_dir::loop));
        }
    }

    // Check consistency
    for (auto i = al.begin(arc_dir::any); i != al.end(arc_dir::any); ++i) {
        REQUIRE(i < al.num_arcs(arc_dir::any));
        if (i >= al.num_arcs(arc_dir::any)) {
            break;
        }
        REQUIRE(al[i].first == i);
    }

    // Add more stuff of various type
    for (unsigned i = 0; i < 5; ++i) {
        al.push(arc_dir::incoming, make(arc_dir::incoming));
        al.push(arc_dir::outgoing, make(arc_dir::outgoing));
        al.push(arc_dir::loop, make(arc_dir::loop));
    }

    // Check consistency
    for (auto type : {arc_dir::any, arc_dir::incoming, arc_dir::loop, arc_dir::outgoing,
                      arc_dir::any_incoming, arc_dir::any_outgoing}) {
        for (auto i = al.begin(type); i != al.end(type); ++i) {
            REQUIRE(al[i].first == i);
            REQUIRE(0 != (al[i].second & type));
        }
    }

    // Remove every odd edge
    const unsigned num_deletions = unsigned(al.num_arcs(arc_dir::any) / 2);
    for (unsigned i = 0; i < num_deletions; ++i) {
        REQUIRE_NOTHROW(al.pop(i));
    }
    REQUIRE_NOTHROW(al.pop(al.num_arcs(arc_dir::any) - 1));

    // Check consistency
    for (auto type : {arc_dir::any, arc_dir::incoming, arc_dir::loop, arc_dir::outgoing,
                      arc_dir::any_incoming, arc_dir::any_outgoing}) {
        for (auto i = al.begin(type); i != al.end(type); ++i) {
            REQUIRE(al[i].first == i);
            REQUIRE(0 != (al[i].second & type));
        }
    }
}