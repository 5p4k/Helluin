//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 25/09/16.
//

#include "catch_nowarn.h"
#include <storage.h>
#include <numeric>

using namespace he;

TEST_CASE("Attach and detach a single storage", "[idx_pool]") {
    storage<int> store;

    { // Store is attached and detached manually
        idx_pool pool;
        REQUIRE_NOTHROW(store.attach_to(pool));
        REQUIRE(store.is_attached());
        REQUIRE(store.pool() == &pool);
        REQUIRE_NOTHROW(store.detach());
        REQUIRE(!store.is_attached());
        REQUIRE(store.pool() == nullptr);
    }

    REQUIRE(!store.is_attached());

    { //Pool is destroyed before the storage (testing ~idx_pool_base)
        idx_pool pool;
        REQUIRE_NOTHROW(store.attach_to(pool));
        REQUIRE(store.is_attached());
        REQUIRE(store.pool() == &pool);
    }
    REQUIRE(!store.is_attached());
    REQUIRE(store.pool() == nullptr);

    { // Re-attaching to two different pools
        idx_pool pool1;
        REQUIRE_NOTHROW(store.attach_to(pool1));
        REQUIRE(store.is_attached());
        REQUIRE(store.pool() == &pool1);
        {
            idx_pool pool2;
            REQUIRE_NOTHROW(store.attach_to(pool2));
            REQUIRE(store.is_attached());
            REQUIRE(store.pool() == &pool2);
        }
        REQUIRE(!store.is_attached());
        REQUIRE(store.pool() == nullptr);
        REQUIRE_NOTHROW(store.attach_to(pool1));
        REQUIRE(store.is_attached());
        REQUIRE(store.pool() == &pool1);
        {
            idx_pool pool2;
            REQUIRE_NOTHROW(store.attach_to(pool2));
            REQUIRE(store.is_attached());
            REQUIRE(store.pool() == &pool2);
            REQUIRE_NOTHROW(store.detach());
            REQUIRE(!store.is_attached());
            REQUIRE(store.pool() == nullptr);
        }
    }

}

TEST_CASE("Synchronization between idx_pool and storage size", "[idx_pool][storage]") {
    static constexpr unsigned num_dummy_elements = 10;
    idx_pool pool;

    REQUIRE(pool.size() == 0);
    REQUIRE(pool.capacity() == 0);

    { // Pool is resized after attaching
        storage<int> store;
        store.attach_to(pool);
        REQUIRE_THROWS(store.at(0));
        for (unsigned i = 0; i < num_dummy_elements; ++i) {
            weak_idx_t allocated_idx = std::numeric_limits<weak_idx_t>::max();
            REQUIRE_NOTHROW(allocated_idx = pool.allocate());
            REQUIRE(allocated_idx == i);
            REQUIRE(pool.is_allocated(i));
            REQUIRE(pool.size() == i + 1);
            REQUIRE(pool.capacity() >= pool.size());
            REQUIRE_NOTHROW(store.at(i));
            REQUIRE_THROWS(store.at(i + 1));
            REQUIRE(store.at(i) == 0);
        }
        store.detach();
        REQUIRE(pool.size() == num_dummy_elements);
        REQUIRE(pool.capacity() >= 0);
    }

    for (unsigned i = 0; i < num_dummy_elements; ++i) {
        REQUIRE_NOTHROW(pool.is_allocated(i));
        REQUIRE(pool.is_allocated(i));
    }

    REQUIRE_NOTHROW(pool.is_allocated(pool.capacity()));

    { // Storage is attached to an already sized pool
        static constexpr int override_default = 5;

        storage<int> store(override_default);
        REQUIRE_NOTHROW(store.attach_to(pool));
        for (unsigned i = 0; i < num_dummy_elements; ++i) {
            REQUIRE_NOTHROW(store.at(i));
            REQUIRE(store[i] == override_default);
        }
        REQUIRE_THROWS(store.at(num_dummy_elements));
    }

    storage<int> store;
    store.attach_to(pool);

    // Now they get destroyed together, it should not throw.
}

TEST_CASE("Testing chi iterator termination condition", "[wrap_iterator][chi]") {
    idx_pool pool;

    // Populate the pool with 10 elements
    static constexpr unsigned num_dummy_elements = 10;
    for (unsigned i = 0; i < num_dummy_elements; ++i) {
        pool.allocate();
    }

    SECTION("Iteration with a vector-based chi iterator") {
        chi_iterator it = pool.begin();
        for (unsigned i = 0; i < num_dummy_elements; ++i) {
            ++it;
        }
        REQUIRE(it == pool.end());
    }

    SECTION("Attaching a storage and asserting the position of the beginning iterator") {
        storage<int> store(1);
        store.attach_to(pool);

        REQUIRE_NOTHROW(*(store.begin()));
        REQUIRE(*(store.begin().inner()) == 0);
        REQUIRE(*(store.begin()) == 1);
    }

    SECTION("Attaching a storage and looping, const_iterator version") {
        storage<int> store(1);
        store.attach_to(pool);

        storage<int>::const_iterator it = store.begin();
        for (unsigned i = 0; i < num_dummy_elements; ++i) {
            REQUIRE_NOTHROW(*it);
            REQUIRE(*it == 1);
            ++it;
        }
        REQUIRE(it == store.end());
    }

    SECTION("Attaching a storage and looping, non-const_iterator version") {
        storage<int> store(1);
        store.attach_to(pool);

        storage<int>::iterator it = store.begin();
        for (unsigned i = 0; i < num_dummy_elements; ++i) {
            REQUIRE_NOTHROW(*it);
            REQUIRE_NOTHROW(*it = 2);
            REQUIRE(*it == 2);
            REQUIRE(store[i] == 2);
            ++it;
        }
        REQUIRE(it == store.end());
    }

    SECTION("Verifying that storage iterators are created around a vector-based chi_iterator") {
        storage<int> store(22);
        store.attach_to(pool);

        REQUIRE_NOTHROW(*(store.begin()));
        REQUIRE_NOTHROW(*(store.begin().inner()));
        REQUIRE(*(store.begin().inner()) == 0);
        pool.destroy(0);
        REQUIRE_NOTHROW(*(store.begin()));
        REQUIRE_NOTHROW(*(store.begin().inner()));
        REQUIRE(*(store.begin().inner()) == 1);

        REQUIRE(pool.begin() == store.begin().inner());
    }
}

TEST_CASE("Removal function of idx_pool, idx_pool in mixed state", "[storage][idx_pool][chi][wrap_iterator]") {
    idx_pool pool;

    // Populate the pool with 10 elements
    static constexpr unsigned num_dummy_elements = 10;
    for (unsigned i = 0; i < num_dummy_elements; ++i) {
        pool.allocate();
    }

    unsigned remaining_elements = num_dummy_elements;
    // Delete all even-indexed elements
    for (unsigned int i = 0; i < num_dummy_elements; ++i) {
        REQUIRE_NOTHROW(pool.is_allocated(i));
        REQUIRE(pool.is_allocated(i));
        if (i % 2 == 0) {
            REQUIRE_NOTHROW(pool.destroy(i));
            REQUIRE_NOTHROW(pool.is_allocated(i));
            REQUIRE(!pool.is_allocated(i));
            REQUIRE(pool.capacity() == num_dummy_elements);
            REQUIRE(pool.size() == --remaining_elements);
        }
    }

    SECTION("Re-insert all the elements") {
        while (remaining_elements < num_dummy_elements) {
            weak_idx_t allocated_index = std::numeric_limits<weak_idx_t>::max();
            REQUIRE_NOTHROW(allocated_index = pool.allocate());
            ++remaining_elements;
            REQUIRE(allocated_index % 2 == 0);
            REQUIRE(allocated_index < num_dummy_elements);
            REQUIRE(pool.is_allocated(allocated_index));
            REQUIRE(pool.size() == remaining_elements);
            REQUIRE(pool.capacity() == num_dummy_elements);
        }

        REQUIRE(pool.allocate() == num_dummy_elements);
    }

    SECTION("Ensure the chi function enumerates correctly the existing elements") {
        unsigned counted_elements = 0;
        std::size_t last_extracted = 0;
        for (std::size_t i : pool) {
            REQUIRE(i < num_dummy_elements);
            REQUIRE(i % 2 == 1);
            if (counted_elements > 0) {
                REQUIRE(last_extracted < i);
            }
            last_extracted = i;
            ++counted_elements;
        }
        REQUIRE(counted_elements == remaining_elements);
    }

    SECTION("Same as before, but uses the is_allocated calls instead of a bool vector") {
        unsigned counted_elements = 0;
        std::size_t last_extracted = 0;
        for (chi_iterator it(pool, false); it != chi_iterator(pool, true); ++it) {
            const std::size_t i = *it;
            REQUIRE(i < num_dummy_elements);
            REQUIRE(i % 2 == 1);
            if (counted_elements > 0) {
                REQUIRE(last_extracted < i);
            }
            last_extracted = i;
            ++counted_elements;
        }
        REQUIRE(counted_elements == remaining_elements);
    }

    SECTION("Cleaning all items") {
        for (std::size_t i : pool) {
            REQUIRE_NOTHROW(pool.destroy(i));
            --remaining_elements;
            REQUIRE(pool.size() == remaining_elements);
        }
        REQUIRE(pool.size() == 0);
        for (std::size_t i : pool) {
            (void) i;
            REQUIRE(false);
        }
        for (; remaining_elements < num_dummy_elements; ++remaining_elements) {
            REQUIRE_NOTHROW(pool.allocate());
        }
        REQUIRE(pool.size() == pool.capacity());
        REQUIRE(pool.size() == num_dummy_elements);
        unsigned seen_idxs = 0;
        for (std::size_t i : pool) {
            REQUIRE(seen_idxs++ == i);
        }
        REQUIRE(seen_idxs == num_dummy_elements);
    }

    SECTION("Attach storage to a pool in a mixed state, delete elements on the fly") {
        storage<unsigned> store(1);
        REQUIRE_NOTHROW(store.attach_to(pool));

        unsigned count = std::numeric_limits<unsigned>::max();
        REQUIRE_NOTHROW(count = std::accumulate(store.begin(), store.end(), 0u));

        REQUIRE(count == pool.size());

        for (std::size_t i = 0; i < num_dummy_elements; ++i) {
            if (pool.is_allocated(i)) {
                REQUIRE_NOTHROW(store.at(i));
                store.at(i) = 4;
                pool.destroy(i);
            }
            REQUIRE_THROWS(store.at(i));
        }

        for (unsigned i = 0; i < num_dummy_elements / 2; ++i) {
            const std::size_t new_elm = pool.allocate();
            REQUIRE(store[new_elm] == 1);
        }
    }

    SECTION("Attach storage in a mixed state, assign and recreate the missing elements") {
        const unsigned total = static_cast<unsigned>(pool.size() * 3 + (pool.capacity() - pool.size()) * 2);
        storage<unsigned> store(2);
        store.attach_to(pool);

        for (auto &elm : store) {
            elm = 3;
        }
        while (pool.size() < pool.capacity()) {
            pool.allocate();
        }

        REQUIRE(std::accumulate(store.begin(), store.end(), 0u) == total);
    }

    SECTION("Testing if storage works as well with proxy elements by allocating vector<bool>") {
        storage<bool> store(false);
        store.attach_to(pool);

        unsigned set_true = 0;
        for (auto b : store) {
            REQUIRE(!b);
            REQUIRE_NOTHROW(b = true);
            REQUIRE(b);
            ++set_true;
        }

        const unsigned int expected_true = (unsigned int) pool.size();
        REQUIRE(expected_true == set_true);

        while (pool.size() < pool.capacity()) {
            pool.allocate();
        }

        unsigned counted_true = 0;
        for (auto const &b : store) {
            if (b) {
                ++counted_true;
            }
        }

        REQUIRE(expected_true == counted_true);
    }

}
