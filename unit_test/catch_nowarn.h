//
//   Copyright 2017 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 28/01/2017.
//

#ifndef PROJECT_CATCH_NOWARN_H
#define PROJECT_CATCH_NOWARN_H

// Thanks to http://stackoverflow.com/a/18463996/1749822.
#ifdef __clang__
#if __has_warning("-Wunevaluated-expression")
#pragma clang diagnostic ignored "-Wunevaluated-expression"
#endif
#if __has_warning("-Wtautological-compare")
#pragma clang diagnostic ignored "-Wtautological-compare"
#endif
#endif

#include <catch.hpp>

#endif //PROJECT_CATCH_NOWARN_H
