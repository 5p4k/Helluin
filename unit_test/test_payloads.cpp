//
//   Copyright 2016 Pietro Saccardi
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
// Created by Pietro Saccardi on 29/01/2017.
//


#include <random>
#include "catch_nowarn.h"
#include <graph_adj_list.h>
#include <proxies.h>
#include <algo.h>
#include <iostream>
#include "utils.h"

using namespace he;

namespace {
    class my_node : public node {
    public:
        int payload;
    };

    using my_graph = proxied_graph<adj_list_graph<>, my_node, int>;
}


TEST_CASE("Strongly typed node proxy", "[proxies][graph_adj_list]") {
    my_graph g;
    populate_graph(g, 10, 20, false);

    SECTION("Test DFS using non-const proxies") {
        my_graph::node_proxy root = g.proxy(g.any_node());

        SECTION("Test dereference and member of operator") {
            root->payload = 5;
            CHECK((*root).payload == 5);
            my_graph::node_const_proxy root_const = root;
            CHECK((*root_const).payload == 5);
            CHECK(root_const->payload == 5);
        }

        // Get an topological order
        std::vector<idx<node>> auto_topo;
        CHECK_NOTHROW(quick_dfs(g, root, [&](graph_base const &, idx<node> n)
        {
            auto_topo.push_back(n);
        }));

        // Do it manually.
        std::vector<idx<node>> manual_topo;
        storage<bool> visited(false);
        visited.attach_to(g.node_pool());
        std::function<void(typename my_graph::node_proxy &)> dfs_step = [&](my_graph::node_proxy &node)
        {
            manual_topo.push_back(node);
            visited[node] = true;
            for (auto neigh : node.gamma(arc_dir::any)) {
                if (not visited[neigh]) {
                    dfs_step(neigh);
                }
            }
        };

        dfs_step(root);

        CHECK(manual_topo.size() == auto_topo.size());
        for (std::size_t i = 0; i < std::min(manual_topo.size(), auto_topo.size()); ++i) {
            CHECK(manual_topo[i] == auto_topo[i]);
        }
    }

    SECTION("Test DFS using const proxies") {
        my_graph::node_const_proxy root = g.proxy(g.any_node());

        // Get an topological order
        std::vector<idx<node>> auto_topo;
        CHECK_NOTHROW(quick_dfs(g, root, [&](graph_base const &, idx<node> n)
        {
            auto_topo.push_back(n);
        }));

        // Do it manually.
        std::vector<idx<node>> manual_topo;
        storage<bool> visited(false);
        visited.attach_to(g.node_pool());
        std::function<void(typename my_graph::node_const_proxy &)> dfs_step = [&](my_graph::node_const_proxy &node)
        {
            manual_topo.push_back(node);
            visited[node] = true;
            for (auto neigh : node.gamma(arc_dir::any)) {
                if (not visited[neigh]) {
                    dfs_step(neigh);
                }
            }
        };

        dfs_step(root);

        CHECK(manual_topo.size() == auto_topo.size());
        for (std::size_t i = 0; i < std::min(manual_topo.size(), auto_topo.size()); ++i) {
            CHECK(manual_topo[i] == auto_topo[i]);
        }
    }


    SECTION("Test ranged loop of nodes and arcs") {
        for (auto idx_ref : g.nodes()) {
            idx_ref.second.payload = int(idx_ref.first);
        }
        for (std::size_t i = 0; i < g.node_pool().size(); ++i) {
            CHECK(g.node_pool().is_allocated(i));
            CHECK(g[g.node_at(i)].payload == i);
        }
        for (auto idx_ref : g.arcs()) {
            idx_ref.second = int(idx_ref.first);
        }
        for (std::size_t i = 0; i < g.arc_pool().size(); ++i) {
            CHECK(g.arc_pool().is_allocated(i));
            CHECK(g[g.arc_at(i)] == i);
        }
    }

    SECTION("Clearing whole graph structure, first arcs, then nodes") {
        while (g.arc_pool().size() > 0) {
            REQUIRE_NOTHROW(g.erase(g.any_arc()));
        }
        while (g.node_pool().size() > 0) {
            REQUIRE_NOTHROW(g.erase(g.any_node()));
        }
    }

    SECTION("Clearing whole graph structure, nodes only") {
        while (g.node_pool().size() > 0) {
            REQUIRE_NOTHROW(g.erase(g.any_node()));
        }
    }
}
